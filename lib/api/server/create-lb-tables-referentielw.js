var server = require('./server');
var ds = server.dataSources.Referentiel;
var lbTables = ['TypePoste', 'Poste', 'Ville', 'Grade', 'Service', 'Administration', 'Fonction', 'Rubrique', 'TypeRubrique', 'TypePrelevement', 'TypePersonne', 'TypeMutuelle', 'Acteur', 'TypeActeur', 'TypeReclamant', 'NatureReclamation', 'TypeEtatReclamation', 'TypeEnvoiReclamation','RoleReclamation', 'SensOperation', 'NatureOperation','Agence', 'Groupe', 'TypeEtatAdhesion', 'TypeEtatAdherent', 'TypeEtatGroupe', 'TeleService', 'TypeEtatTeleService'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});


