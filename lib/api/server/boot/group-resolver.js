module.exports = function(app) {
  var Role = app.models.Role;
  var GroupDAO = app.models.AppGroup;
  var RoleDAO = app.models.AppRole;
  var AppUserDAO = app.models.AppUser;
  

  GroupDAO.find(function(err, groups){
      if(!groups) return;
      for (i = 0; i < groups.length; i++) {
        if(groups[i] && groups[i].name) {

          Role.registerResolver(groups[i].name, function(role, context, cb) {
                           
                function reject() {
                  process.nextTick(function() {
                    cb(null, false);
                  });
                }
  
                var userId = context.accessToken.userId;

                if (!userId) {
                  return reject();
                }


                AppUserDAO.findById( userId ,function(err,user)  {

                    if(err || !user){
                      return reject();
                    }else{

                    user.appRoles(function(err,roles){
                        console.log(roles);
                        if(err)
                          return cb(null,false);
                        
                        for (i = 0; i < roles.length; i++) {
                             console.log(roles[i]);
                             if (roles[i].id == role){
                                return cb(null, true);

                             }

                        } 
                       return cb(null, false);
                    });
                    

                  } 


                });



 
          });
         
         //end register
         //console.log("end");
       }//end if
      }//end for
  });

};
