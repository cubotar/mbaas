'use strict';

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  var app = require('../../server/server');
  server.enableAuth();
  server.use(router);

  //log a user out
  app.get('/logout', function(req, res, next) {
  console.log('hello');
  if (!req.accessToken) return res.sendStatus(401); //return 401:unauthorized if accessToken is not present
  User.logout(req.accessToken.id, function(err) {
    if (err) return next(err);
    res.redirect('/'); //on successful logout, redirect
  });
 });


};
