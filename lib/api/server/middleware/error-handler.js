const createLogger = require('elasticsearch-logger');
var config = require('../config')['elasticsearch'];
const logger = createLogger({ host : config['apiUrl'], index :config['loggerIndexName']});



module.exports = function() {
  return function errorHandler(err,req, res, next) {
     if(err){
        logger.error(err);
     }
     next(err,req, res, next);
  };
};
