
// var config = require('../config')
// var loopback = require('loopback');
// var apiConfigModel = require('../config/model-module.json');
// var Client = require('node-rest-client').Client;
// var client = new Client();

var LoopBackContext = require('loopback-context');

var canPass = function ( url, groups , guard ){
   return canURLPass(url,guard) && canTimePass(guard) && canDayPass(guard) && canGroupPass(groups , guard);
}

var canURLPass = function(url , guard ){
     // check time && date
    if(!guard || !guard.apiRegExp || !url)
        return false;
    var re = new RegExp(guard.apiRegExp);
    
    return re.test(url);
    
}

var canGroupPass = function(groups , guard){
    if(guard==null || guard.groups ==null || guard.groups.contains("ALL")){
        return true;
    }   
    return true;

}
var canTimePass = function(guard){
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var currentTime = h+':'+m;

    if(guard == null || guard.from ==null || guard.to == null){
        return true;
    }else if( currentTime > guard.to && currentTime < guard.from ){
        return true;
    } else {
        return false;
    }
}

var canDayPass = function(guard){
    if(!guard || !guard.days) return false;

    var d = new Date();
    var weekday = new Array(7);
    weekday[0] =  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    
    var n = weekday[d.getDay()];
    return guard.days.contains(n);
}

module.exports = function() {
  return function tracker(req, res, next) {
    return next();
    var app = req.app;
    var guardDAO = app.models.Guard;
    var url = req.url;

    guardDAO.find(function(err, guards){
        console.log(guards);
        console.log(url);
        
        //Getting current user group
        var groups =[];
        //https://loopback.io/doc/en/lb2/Using-current-context.html
        // End getting current user group
        for(var i =0; i < guards.length ; i++){
            if(canPass(url,groups,guards[i])){
                next();
            }
        }
        var err = new Error('Unauthorized request...');
        err.status = 401;
        next(err);

    });
    
  };
};