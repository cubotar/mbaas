
var config = require('../config')
var loopback = require('loopback');
var apiConfigModel = require('../config/model-module.json');
var Client = require('node-rest-client').Client;
var client = new Client();

module.exports = function() {
  return function tracker(req, res, next) {
    config.reportingApiRoot = "/api/(?!Activites)(.*)$";
    var reg = new RegExp(config.reportingApiRoot);
    if (reg.test(req.url)){
      var app = req.app;
      var userId = req.accessToken ? req.accessToken.userId : "unauthenticated";
      var activity = {};
      activity.url = req.url;
      activity.ip  = req.connection.remoteAddress;
      activity.ipForwarded = req.headers['x-forwarded-for']
      activity.date = new Date();
      activity.method = req.method;
      activity.userId= userId;
      var apiName = activity.url.split('/')[2];
      apiName = apiName.split("?")[0];
      if(apiName && apiConfigModel[apiName]){
        activity.module = apiConfigModel[apiName];
      }else{
        activity.module ="NONE";
      }
      var start = process.hrtime();
      res.once('finish', function() {
        var diff = process.hrtime(start);
        var ms = diff[0] * 1e3 + diff[1] * 1e-6;
        activity.responseTime=ms; 
        var ActiviteDAO = app.models.Activite;
        
        // PORT ACTIVITY TO ELASTICSEARCH
        var postUrl = config.elasticsearch.apiUrl+"/"+config.elasticsearch.indexName+"/"+"activities";
        
          var args = {
              data: activity,
              headers: { "Content-Type": "application/json" }
          };

          client.post(postUrl, args, function (data, response) {
          });
        // ActiviteDAO.create(activity,function(err, a ){
        //     if(err){
        //       console.log(err);
        //     }
        // })
      });
    } 
    next();
  };
};
