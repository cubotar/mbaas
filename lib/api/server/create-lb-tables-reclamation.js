var server = require('./server');
var ds = server.dataSources.Reclamation;
var lbTables = ['Reclamation', 'Reclamant', 'EtatReclamation', 'PieceJointe', 'ReponseReclamation'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});


