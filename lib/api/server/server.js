'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();
var express = require('express');
var config = require('./config');
var bunyan = require('bunyan');
var dash = require('appmetrics-dash').monitor();


/*app.enable('trust proxy');
app.use("trust proxy");
app.set("trust proxy", 1);*/


var log = bunyan.createLogger({
    name: 'tgr',
    streams: [config.logger]
});


// Logger global 
app.logger = log;



app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    log.info('Web server listening at: %s', baseUrl);
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
    //server /api dir as a static ressources in order to get photo for appuser
    app.use(express.static('public'));
    app.use('/api', express.static('/api'));
    app.use('html', express.static('html'));

    //enlarge the size limit of http body parser
    var bodyParser = require('body-parser');
    app.use(bodyParser.json({limit: "50mb"}));
    app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
  });
};


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});

// this middleware is invoked in the "routes" phase
app.use('/status', function(req, res, next) {
  res.json({ running: true });
});
