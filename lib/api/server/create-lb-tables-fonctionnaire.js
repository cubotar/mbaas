var server = require('./server');
var ds = server.dataSources.Fonctionnaire;
var lbTables = ['Personne', 'SituationAdministrative' ,'Pension', 'Conjoint', 'Enfant', 'AllocationFamilliale', 'SituationPecuniere','SituationPecuniereDetail','Mutuelle','Igr','Prelevement','Reglement'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});
