var server = require('./server');
var ds = server.dataSources.Fonctionnaire;
var lbTables = ['Personne', 'SituationAdministrative' ,'Pension', 'Conjoint', 'Enfant', 'AllocationFamilliale', 'SituationPecuniere','SituationPecuniereDetail','Mutuelle','Igr','Prelevement','ValidationService', 'Reglement'];
ds.autoupdate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] updated in ', ds.adapter.name);
  ds.disconnect();
});
