'use strict';

module.exports = {
  sendError: function (msg, httpcode, cb) {
     var error = new Error();
     error.message = msg;
     error.statusCode = httpcode;
     cb(null, error);

  }
};



