select date_part('month', date) as dateOperation,
sensoperationid,
       CASE  WHEN sensoperationid=3 THEN 'CREDIT'
            WHEN sensoperationid=4 THEN 'DEBIT'     
       END as sensOperation
,
avg(montant) as avgOperation,
compteid as compte
  from operation
where date >  (current_date - interval '6 months') and compteid='310810100011200804360147'
group by date_part('month', date),sensoperationid, compteid
order by date_part('month', date),sensoperationid, compteid
;





create view banqueGraphe as
select date_part('month', date) as dateOperation,
sensoperationid,
       CASE  WHEN sensoperationid=3 THEN 'CREDIT'
            WHEN sensoperationid=4 THEN 'DEBIT'     
       END as sensOperation
,
avg(montant) as avgOperation,
compteid as compte
  from operation
where date >  (current_date - interval '6 months') and compteid='310810100011200804360147'
group by date_part('month', date),sensoperationid, compteid
order by date_part('month', date),sensoperationid, compteid
;




select * from banqueGraphe
go

