'use strict';

module.exports = function(Rubrique) {
   Rubrique.validatesUniquenessOf('code', {message: 'code is not unique'});
};
