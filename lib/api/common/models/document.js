'use strict';


module.exports = function(Document) {
   
      Document.editerDocument = function(code, cb) {


      Document.find({where:{code: code}, include: {relation: 'paragraphes'}}, function (err, document) {

        if (err)
          cb(null,err);

         if (document){
             cb(null, document);
         }else{
	     cb(null, 'Document non trouve!');
         }

        });
   }


       Document.remoteMethod('editerDocument', {
          accepts: {arg: 'code', type: 'string'},
          http: {path: '', verb: 'get'},
          returns: {arg: 'document', type: 'array'}
       });


};
