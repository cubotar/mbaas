'use strict';

module.exports = function(Sensoperation) {
  Sensoperation.validatesUniquenessOf('code', {message: 'code is not unique'});
};
