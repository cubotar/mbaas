'use strict';

module.exports = function(Typeetatteleservice) {
  Typeetatteleservice.validatesUniquenessOf('code', {message: 'code is not unique'});
};
