'use strict';

module.exports = function(Fonction) {
  Fonction.validatesUniquenessOf('code', {message: 'code is not unique'});
};
