'use strict';

module.exports = function(Typeetatgroupe) {
  Typeetatgroupe.validatesUniquenessOf('code', {message: 'code is not unique'});
};
