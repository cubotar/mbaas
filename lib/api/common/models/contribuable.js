'use strict';

module.exports = function(Contribuable) {

   //get solde of a rib id
   Contribuable.consulterArticlesARegler = function(nature, reference, montant, annee, cb) {

       var montant = montant;
       if (annee && nature && reference && montant){
            if(reference.length === 8){
               var a = annee.substr(annee.length - 1);
               var codebarre = nature+''+reference+''+a;
               Contribuable.generateKey(codebarre,  function (err, key) {
                  if (err)
          	   cb(null,err);
                  if (key != null){
                     var ref = codebarre + key;
                     Contribuable.getResultPEL(ref, montant, function (err, articles) {
                  	if (err)
                          cb(null,err);
                        if (articles != null)
                          cb(null, articles);
                      });
                  }
               });
              }else{
                cb(null, "Reference incorrecte!");
              }
         }else{
             cb(null, "Parametres invalides!");
         }
   }


  Contribuable.remoteMethod('consulterArticlesARegler', {
          accepts: [{arg: 'nature', type: 'string'}, {arg: 'reference', type: 'string'}, {arg: 'montant', type: 'string'}, {arg: 'annee', type: 'string'}],
          http: {path: '', verb: 'get'},
          returns: {arg: 'consulterArticlesARegler', type: 'string'}
  });
  
};
