'use strict';

module.exports = function(Groupe) {
 Groupe.validatesUniquenessOf('code', {message: 'code is not unique'});
};
