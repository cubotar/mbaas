'use strict';


module.exports = function(Typeville) {
    Typeville.validatesUniquenessOf('code', {message: 'code is not unique'});

   
    Typeville.villesParTypeEtNature = function(codetype, codenature, cb) {
       //returns array of objects
       if (!codetype)
         cb(null,'code type requis!');
       else{
       Typeville.findOne({where:{code:codetype}, include: {relation: 'villes', scope:{include: {relation: 'natureVille'}}}}, function (err, typeville) {
        var list = [];
        if (err)
          cb(null,err);
         if (typeville){
             var villes = JSON.parse(JSON.stringify(typeville)).villes;
             villes.forEach(function(ville) {
           
                if (codenature && ville.natureVille.code === codenature){
                  delete ville.natureVille;
                  list.push(ville);
                }else if (!codenature){
                  delete ville.natureVille;
                  list.push(ville);     
                }
              });
             cb(null, list);
         }else{
             cb(null, 'pas de donnees!');
         }
        });
       }
   }

    Typeville.remoteMethod('villesParTypeEtNature', {
          accepts: [{arg: 'codetype', type: 'string'},{arg: 'codenature', type: 'string'}],
          http: {path: '', verb: 'get'},
          description: ["Liste de villes pour un type de ville choisi, et une nature de ville choisie : ",
                         "Entrer le code type ville et le code nature ville"],
          returns: {arg: 'villes', type: 'array'}
    });


};
