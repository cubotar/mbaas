'use strict';

module.exports = function(Typeacteur) {
   Typeacteur.validatesUniquenessOf('code', {message: 'code is not unique'});

   Typeacteur.acteurs = function(code, cb) {
       //returns array of objects
       Typeacteur.findOne({where:{code:code}, include: {relation: 'acteurs', scope: {fields: ['id', 'code', 'libelle']}}}, function (err, typeacteur) {
        if (err)
          cb(null,err);
        if (typeacteur){
           var acteurs = JSON.parse(JSON.stringify(typeacteur)).acteurs;
           cb(null, acteurs);
          }
        });
   }


    Typeacteur.remoteMethod('acteurs', {
          accepts: {arg: 'code', type: 'string'},
          http: {path: '', verb: 'get'},
          description: ["Liste des acteurs pour unn type acteur choisi :",
                         "Entrer le code du type acteur"],
          returns: {arg: 'acteurs', type: 'array'}
    });

  
};
