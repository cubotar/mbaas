'use strict';


var thisRoot = "http://10.96.8.79:10039";
var rss_url =thisRoot+"/wps/wcm/connect/TGR-Intranet/Accueil/Fait?srv=cmpnt&source=library&cmpntname=TGR-Intranet/AtomFeedMenu";
var request = require('request');
var parseString = require('xml2js').parseString;

module.exports = function(Actualite) {

Actualite.getNews = function(cb){
console.log("calling getNews");
var results = [];
request(rss_url,function(err, response, body){
    console.log("Calling request...");
    if(err) cb(null,false);
        parseString(body, function (err, result) {

                if(err) cb(null,false);

                var entries = result.feed.entry;
                for(var i=0 ; i < entries.length ; i++) {
                        var thisTitle = entries[i].title[0];
                        var thisContent = entries[i].content[0];
                        var thisImageURL = thisRoot+entries[i].image[0];
                        var thisLink = entries[i].link[0]['$']['href'];
                        if (thisLink.indexOf('path') != -1){
                             thisLink = thisLink.replace("portal/?urile=wcm:path:/wps/", "");
                             thisLink = thisLink.replace("portal/intranet?urile=wcm:path:/wps/", "");
                        }
                        thisLink  = thisLink.replace("10.96.8.79:10039", "www.tgr.gov.ma");
                        results.push({ title: thisTitle  , content : thisContent , imageURL : thisImageURL, link : thisLink });
                }
                cb(null,results);
        });

});

};	


Actualite.remoteMethod('getNews' , {
	returns : { arg:'news' , type:'array'},
	http: { path : '/getNews' , verb :'get'}	
});




};
