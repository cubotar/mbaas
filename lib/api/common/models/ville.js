'use strict';

module.exports = function(Ville) {
  Ville.validatesUniquenessOf('code', {message: 'code is not unique'});

  Ville.acteurs = function(codeville, codetypeacteur, cb) {
       //returns array of objects
       var list= [];
       Ville.findOne({where:{code:codeville}, include: {relation: 'acteurs', scope: {fields: ['id', 'code', 'libelle'], include: 'typeActeur'}}}, function (err, ville) {
        if (err)
          //console.log(err);
          cb(null,err);
        if (ville){
            var acteurs = JSON.parse(JSON.stringify(ville)).acteurs;
            acteurs.forEach(function(acteur) {
               if (acteur.typeActeur && acteur.typeActeur.code === codetypeacteur){
                delete acteur.typeActeur;
                list.push(acteur)
               }
           });
           cb(null, list);

        }else{
             cb(null, 'pas de donnees!');
         }
        });
   }

    /*Ville.postes = function(codeville, cb) {
       //returns array of objects
       //include: {relation: 'postes', scope:{include: {relation: 'ville', scope: {where: {code: 'NATIONAL'}}}}}}
       var list = [];
       Ville.findOne({where:{code:codeville}, include: {relation: 'postes', scope:{fields: ['id', 'code', 'libelle', 'ville']}}}, function (err, ville) {
        if (err)
          cb(null,err);
        if (ville){
           var postes = JSON.parse(JSON.stringify(ville)).postes;
           cb(null, postes);
        }else{
             cb(null, 'pas de donnees!');
         }
        });
   }

  */


   Ville.remoteMethod('acteurs', {
          accepts: [{arg: 'codeville', type: 'string'}, {arg: 'codetypeacteur', type: 'string'}], 
          http: {path: '', verb: 'get'},
          description: ["Liste acteurs pour une ville choisie et unn type acteur choisi: ",
                         "Entrer le code ville et le code type acteur."],
          returns: {arg: 'acteurs', type: 'array'}
    });

    /*Ville.remoteMethod('postes', {
          accepts: {arg: 'codeville', type: 'string'},
          http: {path: '', verb: 'get'},
          description: ["Liste acteurs pour une ville choisie: ",
                         "Entrer le code ville."],
          returns: {arg: 'acteurs', type: 'array'}
    });*/


};
