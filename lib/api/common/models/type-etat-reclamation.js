'use strict';

module.exports = function(Typeetatreclamation) {
  Typeetatreclamation.validatesUniquenessOf('code', {message: 'code is not unique'});
};
