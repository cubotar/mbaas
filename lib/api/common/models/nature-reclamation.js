'use strict';

module.exports = function(Naturereclamation) {
 Naturereclamation.validatesUniquenessOf('code', {message: 'code is not unique'});
};
