'use strict';

var app = require('../../server/server');
var parallel = require('async/parallel');


module.exports = function (Reclamation) {





    Reclamation.observe('after save', function afterSave(ctx, next) {
        if (ctx.hookState._piecesJointes) {
            ctx.instance.pieceJointes.create(ctx.hookState._piecesJointes, function (err, pieces) {
                if (err)
                    next(err);
                else
                    next();
            });
        } else {
            next();
        }


        //attacher la reclamation cree la  r�ponse R�clamation automatique

        Reclamation.findOne({ where: { reference: ctx.hookState._reference } }, function (err, reclamationPersisted) {
            if (err) {
                next(err);
            }
            else {

                if (reclamationPersisted && reclamationPersisted.id) {
                    ctx.hookState._reponseReclamation.updateAttributes({ reclamationId: reclamationPersisted.id }, function (err, reponseUpdated) {
                        if (err) {
                           // next(err);
                            console.log("Failed to attach r�ponse reclamation !!!!");
                        } else
                            console.log("Success to attach r�ponse reclamation !!!!");
                    });
                }
            }
        });





        // after save envoi de mail
        console.log(ctx.hookState._reclamant);
        if (ctx.hookState._reclamant) {
            var reclamant = ctx.hookState._reclamant;

            if (ctx.hookState._reclamant.mail) {

                var msgText1 = "<br>La TGR tient � vous informer que votre r�clamation a �t� prise  en charge. N� de r�f�rence :";

                msgText1 = msgText1 + "<br> <strong><font face='Tahoma' size=2>" + ctx.hookState._reference + "<strong></font>"


                msgText1 = msgText1 + "<br>Le n�cessaire sera fait pour traiter votre r�clamation dans les plus brefs d�lais.";
                msgText1 = msgText1 + "<br>Pour tout compl�ment d�information, nous restons � votre disposition.<br><br>";
                msgText1 = msgText1 + "Votre satisfaction est notre priorit�.<br><br><br><br>";
                msgText1 = msgText1 + "<font face='Tahoma' size=1 color='#606060'>� Ceci est un message de notification. Merci de ne pas y r�pondre  �</font><br>";
                msgText1 = msgText1 + "<font face='Tahoma' size=1 color='#606060'>Pour faire le suivi de votre r�clamation, entrer votre r�f�rence fournie ci-dessus </font>";



                Reclamation.app.models.Mail.send({
                    to: ctx.hookState._reclamant.mail,
                    from: 'portail@tgr.gov.ma',
                    subject: '[TGR Réclamation] : Accusé de récéption',
                    text: 'cliquez ',
                    html: '' + msgText1


                }, function (err, mail) {
                    if (!err) {
                        console.log('email sent!');
                        console.log(mail);

                    } else {
                        console.log('email not sent!');
                        console.log(mail);

                    }
                });


            }
        }

    });



    Reclamation.observe('before save', function updateTimestamp(ctx, next) {

        if (ctx.instance._piecesJointes) {
            // Saving piece Jointes for after save hook
            ctx.hookState._piecesJointes = ctx.instance._piecesJointes;
            ctx.instance.unsetAttribute('_piecesJointes');
        }

        if (ctx.instance._reclamant) {
            // Saving _reclamant for after save hook
            ctx.hookState._reclamant = ctx.instance._reclamant;

        }

        //todo ajouter le poste par d�faut en cas ou le poste est non renseign�
        if (!ctx.instance.posteId) {
            app.models.Poste.findOne({ where: { code: 'N633' } }, function (err, poste) {
                if (err) {
                    callback(err, null);
                }
                else if (poste && poste.id != null) {
                    console.log("poste.id " + poste.id);
                    ctx.instance.posteId = poste.id;
                }
            });
        }

        // reference

        console.log("reference" + ctx.instance.reference);
        var date = new Date();
        console.log(date);
        var timeMS = date.getTime();
        console.log(timeMS);
        var referenceReclam = "refMobile".concat(timeMS);
        ctx.instance.reference = referenceReclam;
        ctx.hookState._reference = referenceReclam;
        parallel({
            async_reclamant: function (callback) {
                // Save _reclamant
                if (ctx.instance._reclamant) {
                    var reclamantDAO = app.models.Reclamant;
                    var reclamantInstance = ctx.instance._reclamant;

                    ctx.instance.unsetAttribute('_reclamant');
                    reclamantDAO.create(reclamantInstance, function (err, r) {
                        if (!err) {
                            callback(null, r);
                        } else {
                            callback(err, null);
                        }
                    });
                } else {
                    callback(null, null);
                }

            },
            async_etat_reclamation: function (callback) {

                if (ctx.instance._etatReclamation) {
                    // Save etat reclamation
                    app.models.TypeEtatReclamation.findOne({ where: { code: 'EN_INSTANCE' } }, function (err, typeEtatRec) {
                        if (err) {
                            callback(err, null);
                        }
                        else {
                            ctx.hookState._etatEnInstanceID = typeEtatRec.id;

                            var etatReclamationDAO = app.models.EtatReclamation;
                            var etatReclamationInstance = ctx.instance._etatReclamation;
                            etatReclamationInstance.typeEtatReclamationId = typeEtatRec.id;
                            ctx.instance.unsetAttribute('_etatReclamation');
                            etatReclamationDAO.create(etatReclamationInstance, function (err, etatRec) {
                                if (!err) {
                                    callback(null, etatRec)
                                } else {
                                    callback(err, null);
                                }
                            });
                        }

                    });
                }
                else {
                    callback(null, null);
                }

            }


        }, function (err, results) {
            if (err) {
                next(err);
            }
            else {
                if (results.async_reclamant) {
                    ctx.instance.reclamant(results.async_reclamant);

                }
                if (results.async_etat_reclamation)
                    ctx.instance.etatReclamation(results.async_etat_reclamation);


                //attacher reponse reclamation en instance befor saving reclamation

                // reponse reclamation
                //Reclamation.attacherReponse = function(ppr, dateEntree,netMensuel,cb) {
                var reponseReclamationDAO = app.models.ReponseReclamation;

                var reponseReclamationInstance = app.models.ReponseReclamation;
                console.log("Votre réclamation est en cours de traitement !");
                reponseReclamationInstance = {
                    "reponse": "Votre réclamation est en cours de traitement !",
                    "date": new Date(),
                    "typeEtatReclamationId": ctx.hookState._etatEnInstanceID

                }

                reponseReclamationDAO.create(reponseReclamationInstance, function (err, reponseR) {
                    if (!err) {
                        //cb(null,reponseR);     
                        ctx.hookState._reponseReclamation = reponseR;
                        ctx.instance.reponseReclamations(reponseR);
                    } else {
                        next(err);
                    }
                });




                next();
            }

        });

    });
}
