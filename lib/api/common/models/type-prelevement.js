'use strict';

module.exports = function(Typeprelevement) {
  Typeprelevement.validatesUniquenessOf('code', {message: 'code is not unique'});
};
