'use strict';

module.exports = function(Administration) {
  Administration.validatesUniquenessOf('code', {message: 'code is not unique'});

};
