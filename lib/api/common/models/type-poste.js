'use strict';


module.exports = function(Typeposte) {
    Typeposte.validatesUniquenessOf('code', {message: 'code is not unique'});

   Typeposte.directionsRegionales = function(cb) {
       //returns array of objects
       Typeposte.find({where:{code:'REGIONAL'}, include: {relation: 'postes', scope: {where:{posteRattachement:''}}}}, function (err, typeposte) {

        if (err)
          cb(null,err);

         if (typeposte != null){
             cb(null, typeposte);
          }

        });

   }
   
   Typeposte.directionsCentrales = function(cb) {

      Typeposte.find({where:{code:'CENTRAL'}, include: {relation: 'postes', scope: {where:{posteRattachement:''}}}}, function (err, typeposte) {

        if (err)
          cb(null,err);

         if (typeposte != null){
             cb(null, typeposte);
          }

        });

   }

   Typeposte.directionsParType = function(code, cb) {

      Typeposte.find({where:{code:code}, include: {relation: 'postes', scope: {where:{posteRattachement:''}}}}, function (err, typeposte) {

        if (err)
          cb(null,err);

         if (typeposte != null){
             cb(null, typeposte);
          }

        });

   }


  Typeposte.remoteMethod('directionsRegionales', {
          accepts: [],
          http: {path: '', verb: 'get'},
          description: "Liste de directions services exterieurs",
          returns: {arg: 'directionsRegionales', type: 'array'}
  });

  Typeposte.remoteMethod('directionsCentrales', {
          accepts: [],
          http: {path: '', verb: 'get'},
          description: "Liste de directions services centraux",
          returns: {arg: 'directionsCentrales', type: 'array'}
  });

  Typeposte.remoteMethod('directionsParType', {
          accepts: [],
          http: {path: '', verb: 'get'},
          description: ["Liste de directions pour un type de poste/service choisi : ",
                         "Entrer le code type poste"],
          returns: {arg: 'directions', type: 'array'}
  });

};
