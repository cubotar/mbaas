'use strict';

module.exports = function(Service) {
  Service.validatesUniquenessOf('code', {message: 'code is not unique'});

};
