'use strict';

module.exports = function(Typeetatadhesion) {
  Typeetatadhesion.validatesUniquenessOf('code', {message: 'code is not unique'});
};
