'use strict';


module.exports = function(Compte) {

   var app = require('../../server/server');

   //get solde of a rib id
   Compte.soldeBancaire = function(rib, cb) {

       Compte.findById(rib,  {include: ['agence', 'typePersonne', 'solde']}, function (err, compte) {
        if (err)
          cb(null,err);

         if (compte != null){
           cb(null, compte);
         }else{
           cb(null, null);
         }

        });//Compte.findById

   }

    Compte.dernieresOperations  = function(rib, cb) {

       Compte.findById(rib, {include: [ {relation: 'operations', scope:{limit:15, order: 'date DESC', include: {relation: 'sensOperation'} } }  , 'agence', 'typePersonne']}, function (err, compte) {
        if (err)
          cb(null,err);


         if (compte != null){
            cb(null, compte);

         }else{
             cb(null, null);

         }

        });//Compte.findById

   }

    
    Compte.entreDatesOperations = function(rib, date1, date2, cb) {

      Compte.findById(rib, {include: [ {relation: 'operations', scope:{where: {date: {gt: new Date(date1), lt: new Date(date2)}}, include: {relation: 'sensOperation'} } }  , 'agence', 'typePersonne']}, function (err, compte) {

        if (err)
          cb(null,err);


         if (compte != null){
          //getEntreDeuxDatesOperations(compte, date1, date2, cb);
           cb(null, compte);

         }

        });//Compte.findById

   }

    Compte.appUser = function(id, cb) {
        var Appuser = app.models.AppUser;   
        Appuser.findOne({where: {norib:rib}}, function (err, appuser) {

         if (err)
          cb(null,err);
         if ( appuser != null){
              cb(null, appuser);
         }//end if
         else{
           cb(null, null);
         }
        });
   }

  Compte.beforeRemote('**', function(context, user, next) {
        //console.log('toto');
	var req = context.req;
        //console.log(req);
	//req.body.date = Date.now();
	//req.body.publisherId = req.accessToken.userId;
	next();
  });


  Compte.observe('access', function(ctx, next) {
    const token = ctx.options && ctx.options.accessToken;
    const userId = token && token.userId;
    const user = userId ? 'user#' + userId : '<anonymous>';
    const modelName = ctx.Model.modelName;
    const scope = ctx.where ? JSON.stringify(ctx.where) : '<all records>';
    //console.log('%s: %s accessed %s:%s', new Date(), user, modelName, scope);

    next();
  });
 
  Compte.createOptionsFromRemotingContext = function(ctx) {
    //var base = this.base.createOptionsFromRemotingContext(ctx);
    //console.log(base);
    //return base;
  };


  //Compte.belongsTo(); 

  Compte.remoteMethod('soldeBancaire', {
          accepts: {arg: 'rib', type: 'string'},
          http: {path: '', verb: 'get'},
          returns: {arg: 'soldeBancaire', type: 'string'}
  });

  Compte.remoteMethod('dernieresOperations', {
          accepts: {arg: 'rib', type: 'string'},
          http: {path: '', verb: 'get'},
          returns: {arg: 'dernieresOperations', type: 'string'}
  });

  Compte.remoteMethod('entreDatesOperations', {
          accepts: [{arg: 'rib', type: 'string'}, {arg: 'date1', type: 'string'}, {arg: 'date2', type: 'string'}],
          http: {path: '', verb: 'get'},
          returns: {arg: 'entreDatesOperations', type: 'string'}
  });

  Compte.remoteMethod('appUser', {
          accepts: {arg: 'id', type: 'string'},
          http: {path: '/:id/appUser', verb: 'get'},
          returns: {arg: 'appUser', type: 'string'}
  });


};


