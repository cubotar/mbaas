'use strict';

module.exports = function(Agence) {
  Agence.validatesUniquenessOf('code', {message: 'code is not unique'});
};
