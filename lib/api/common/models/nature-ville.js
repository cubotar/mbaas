'use strict';


module.exports = function(Natureville) {
    Natureville.validatesUniquenessOf('code', {message: 'code is not unique'});

   
    Natureville.villesParTypeEtNature = function(codetype, codenature, cb) {
       //returns array of objects
       Natureville.findOne({where:{code:codenature}, include: {relation: 'villes', scope:{include: {relation: 'typeVille', scope: {where: {code: codetype}}}}}}, function (err, natureville) {

        if (err)
          cb(null,err);
         if (natureville != null){
             var villes = JSON.parse(JSON.stringify(natureville)).villes;
             villes.forEach(function(ville) {
                delete ville.typeVille;
              });
             cb(null, villes);
         }else{
             cb(null, 'pas de donnees!');
         }
        });

   }

    Natureville.remoteMethod('villesParTypeEtNature', {
          accepts: [{arg: 'codetype', type: 'string'},{arg: 'codenature', type: 'string'}],
          http: {path: '', verb: 'get'},
          description: ["Liste de villes pour un type de ville choisi, et une nature de ville choisie : ",
                         "Entrer le code type ville et le code nature ville"],
          returns: {arg: 'villes', type: 'array'}
    });


};
