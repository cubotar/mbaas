'use strict';

module.exports = function(Teleservice) {
  Teleservice.validatesUniquenessOf('code', {message: 'code is not unique'});
};
