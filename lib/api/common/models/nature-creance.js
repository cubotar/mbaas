'use strict';

module.exports = function(Naturecreance) {
  Naturecreance.validatesUniquenessOf('code', {message: 'code is not unique'});
};
