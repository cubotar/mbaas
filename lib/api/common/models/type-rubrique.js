'use strict';

module.exports = function(Typerubrique) {
  Typerubrique.validatesUniquenessOf('code', {message: 'code is not unique'});
};
