'use strict';

module.exports = function(Rolereclamation) {
    Rolereclamation.validatesUniquenessOf('code', {message: 'code is not unique'});
};
