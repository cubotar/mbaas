'use strict';

module.exports = function(Typemutuelle) {
  Typemutuelle.validatesUniquenessOf('code', {message: 'code is not unique'});
};
