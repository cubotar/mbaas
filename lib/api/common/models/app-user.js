'use strict';

var sha1 = require('sha1');
var Promise = require('bluebird');
var openldapssha = require('openldap_ssha');
const fs = require('fs');
var http = require('http');
var serverURL = 'https://api.tgr.gov.ma';
var utils = require('../../node_modules/loopback/lib/utils');
var assert = require('assert');


function createPromiseCallback() {
  var cb;
  var promise = new Promise(function (resolve, reject) {
    cb = function (err, data) {
      if (err) return reject(err);
      return resolve(data);
    };
  });
  cb.promise = promise;
  return cb;
}


function createCallback() {
  var cb;
  var promise = new Promise(function (resolve, reject) {
    cb = function (err, data) {
      if (err) return resolve(err);
      return resolve(data);
    };
  });
  cb.promise = promise;
  return cb;
}


// COMPUTING SALTED SHA1 FROM PLAIN
function computeSaltedHash(plain, salt) {
  var hashed64PWD;
  openldapssha.ssha_pass(plain, salt, function (err, hash) {
    var hashedPWD = hash.replace("{SSHA}", "{SHA}");
    hashed64PWD = new Buffer(hashedPWD).toString('base64');
    //hashed64PWD=hashedPWD;
  });
  return hashed64PWD;
}

// COMPUTING SALTED SHA1 FROM PLAIN TO BE STORED IN THE LDAP DIRECTORY
function computeSaltedHashToLDAP(plain, salt) {
  var hashed64PWD;
  openldapssha.ssha_pass(plain, salt, function (err, hash) {
    hashed64PWD = hash.replace("{SSHA}", "{SHA}");
  });
  return hashed64PWD;
}


module.exports = function (Appuser) {

  var app = require('../../server/server');

  Appuser.prototype.hasPassword = function (plain, fn) {
    fn = fn || createPromiseCallback();
    if (this.password && plain && computeSaltedHash(plain, "") == this.password) {
      fn(null, true);
    } else {
      fn(null, false);
    }
    return fn.promise;
  };

  Appuser.hashPassword = function (plain) {
    this.validatePassword(plain);
    return computeSaltedHashToLDAP(plain, "");
  };


  Appuser.on('resetPasswordRequest', function (data, cb) {
    var userId = data.accessToken.userId;
    var oldPassword = data.options.oldPassword;
    var newPassword = data.options.newPassword;
    var email = data.options.email;

    if (oldPassword) {
      Appuser.changePassword(userId, oldPassword, newPassword, function (err, user) {
        if (err) {
          //cb = createCallback();
          //return cb(null, err);
          //assert(err);
          return console.log(err);
        }
        if (user) {
          console.log('success');
          cb(null, user);
        }
      });
    } else if (newPassword) {
      Appuser.setPassword(userId, newPassword, function (err, user) {
        if (err) {
          //cb = createCallback();
          //return cb(null, err);
          //assert(err);
          return console.log(err);
        }
        if (user) {
          console.log('success');
          cb(null, user);
        }
      });
    } else {

      Appuser.resetPassword(data, function (err, user) {
        var url = 'https://api.tgr.gov.ma/api/AppUsers/reset';
        var html = 'Click <a href="' - url - '?access_token=' -
          data.accessToken.id - '">here</a> to reset your password';

        app.models.Mail.send({
          to: data.options.email,
          from: 'portail@tgr.gov.ma',
          subject: 'Réitialisation du mot de passe sur Application Mobile TGR',
          text: 'cliquez ',
          html: '' + html
        }, function (err, mail) {
          if (!err) {
            console.log('email sent!');
            console.log(mail);

          } else {
            console.log('email not sent!');
            console.log(mail);
          }
        });

        if (err) {
          //cb = createCallback();
          cb(null, err);
          //assert(err);
          //return console.log(err);
        }
        if (user) {
          console.log('success');
          cb(null, user);
        }
      });

    }
    //console.log("do something"+JSON.stringify(data));
  });



  //Application Logic

  /***********************************/
  /** SituationAdministrative Logic **/
  /***********************************/

  Appuser.editionSituationAdministrative = function (id, cb) {
    //app.models.SituationAdministrative.findById( id, {include: ['ville', 'service', 'administration', 'grade', 'fonction', 'enfants', 'conjoints', 'reglements', 'igrs', 'allocationsfamiliales', 'mutuelles', 'prelevements', 'validationServices']}, function (err, situationadmin) {
    
      Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      if (!appuser.employeenumber)
        cb(null, 'AppUser employeenumber missing!');
      else {
      app.models.SituationAdministrative.findById(appuser.employeenumber, { include: ['ville', 'service', 'administration', 'grade', 'fonction', { relation: 'enfants', scope: { include: { relation: 'conjoint' } } }, 'conjoints', 'reglements', 'igrs', 'allocationsfamiliales', { relation: 'mutuelles', scope: { include: { relation: 'typeMutuelle' } } }, { relation: 'prelevements', scope: { include: { relation: 'rubrique' } } }, 'validationServices'] }, function (err, situationadmin) {

      if (err)
        cb(null, err);
      if (situationadmin) {
        cb(null, situationadmin);
      } else {
        cb(null, null);
      }
    });
   }
   });
  }

  Appuser.attestationSalaire = function (id, cb) {

    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      if (!appuser.employeenumber)
        cb(null, 'AppUser employeenumber missing!');
      else {
    app.models.SituationAdministrative.findById(appuser.employeenumber, { include: [{ relation: 'situationPecuniere', scope: { include: { relation: 'situationPecuniereDetails', scope: { include: ['typeRubrique', 'rubrique'] } } } }, 'ville', 'service', 'administration', 'grade', 'fonction', 'enfants'] }, function (err, situationadmin) {

      if (err)
        cb(null, err);
      if (situationadmin) {
        cb(null, situationadmin);
      }

    });//Situationadministrative.findById
   }
  });
  }

  Appuser.situationPrelevements = function (id, cb) {
    app.models.SituationAdministrative.findById(id, { include: [{ relation: 'prelevements', scope: { include: { relation: 'rubrique' } } }, 'ville', 'service', 'administration', 'grade', 'fonction'] }, function (err, situationadmin) {

      if (err)
        cb(null, err);
      if (situationadmin) {
        cb(null, situationadmin);
      }

    });//Situationadministrative.findById
  }



  /***********************************/
  /** Banque Logic **/
  /***********************************/
  Appuser.soldeBancaire = function (id, rib, cb) {
    var status = false;
    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      if (appuser.norib)
        appuser.norib.forEach(function (no) {
          if (no === rib) status = true;
        });
      if (!status) {
        cb(null, 'Autorization required!');
      }
      else {
        app.models.Compte.findById(rib, { include: ['agence', 'typePersonne', 'solde'] }, function (err, compte) {
          if (err)
            cb(null, err);
          if (compte) {
            cb(null, compte);
          } else {
            cb(null, null);
          }
        });//Compte.findById
      }
    });//end findById
  }

  Appuser.dernieresOperations = function (id, rib, cb) {
    var status = false;
    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      if (appuser.norib)
        appuser.norib.forEach(function (no) {
          if (no === rib) status = true;
        });
      if (!status) {
        cb(null, 'Autorization required!');
        return;
      }
      else {
        app.models.Compte.findById(rib, { include: [{ relation: 'operations', scope: { limit: 15, order: 'date DESC', include: { relation: 'sensOperation' } } }, 'agence', 'typePersonne'] }, function (err, compte) {
          if (err)
            cb(null, err);

          if (compte != null) {
            cb(null, compte);
          } else {
            cb(null, null);
          }
        });//Compte.findById
      }
    });//end findById
  }


  Appuser.entreDatesOperations = function (id, rib, date1, date2, cb) {
    var status = false;
    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      if (appuser.norib) {
        appuser.norib.forEach(function (no) {
          if (no === rib) status = true;
        });
      }
      if (!status)
        cb(null, 'Autorization required!');
      else {
        app.models.Compte.findById(rib, { include: [{ relation: 'operations', scope: { where: { date: { gt: new Date(date1), lt: new Date(date2) } }, include: { relation: 'sensOperation' } } }, 'agence', 'typePersonne'] }, function (err, compte) {
          if (err)
            cb(null, err);

          if (compte) {
            cb(null, compte);
          } else {
            cb(null, null);
          }
        });//Compte.findById
      }
    });//end findById
  }


  //recupererDonneesGraphe id= compteid
  Appuser.recupererDonneesGraphe = function (id, rib, cb) {
    var status = false;
    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      if (appuser.norib) {
        appuser.norib.forEach(function (no) {
          if (no === rib) status = true;
        });
      }
      if (!status)
        cb(null, 'Autorization required!');
      else {
        app.models.BanqueGraphe.find({ where: { 'compte': rib } }, function (err, data) {
          if (err)
            cb(null, err);

          if (data) {
            console.log("data " + data);
            cb(null, data);
          } else {
            cb(null, null);
          }
        });
      }
    }); //findByID user
  }

  /***********************************/
  /** SituationFiscale Logic **/
  /***********************************/

  Appuser.consulterArticlesSoldes = function (id, codecontribuable, cb) {
    var status = false;
    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      var contribuables = appuser.codecontribuable;
      contribuables.forEach(function (contribuable) {
        var i1 = contribuable.indexOf('|') + 1;
        var i2 = contribuable.lenght;
        var ct = contribuable.substring(i1, i2);
        if (ct === codecontribuable) status = true;
      });
      if (!status)
        cb(null, 'Autorization required!');
      else {
        app.models.SituationFiscale.getResultS(codecontribuable, function (err, articles) {
          if (err)
            cb(null, err);
          if (articles) {
            cb(null, articles);
          } else {
            cb(null, null);
          }
        });
      }
    });//end findById
  }


  Appuser.consulterArticlesNonSoldes = function (id, codecontribuable, cb) {
    var status = false;
    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      var contribuables = appuser.codecontribuable;
      contribuables.forEach(function (contribuable) {
        var i1 = contribuable.indexOf('|') + 1;
        var i2 = contribuable.lenght;
        var ct = contribuable.substring(i1, i2);
        if (ct === codecontribuable) status = true;
      });
      if (!status)
        cb(null, 'Autorization required!');
      else {
        app.models.SituationFiscale.getResultNS(codecontribuable, function (err, articles) {
          if (err)
            cb(null, err);
          if (articles) {
            cb(null, articles);
          } else {
            cb(null, null);
          }
        });
      }
    });//end findById
  }


  /***********************************/
  /** GID_Fournisseurs Logic        **/
  /***********************************/
  Appuser.listeActesFournisseurs = function (id, cb) {
    Appuser.findById(id, function (err, appuser) {
      if (err)
        cb(null, err);
      else {
        app.models.Tiers.findOne({ where: { 'login': id }, include: ['mDs', 'mPs'] }, function (err, tiersActesFournisseurs) {
          // app.models.Tiers.findOne({where:{'login': id }},{include: ['mDs', 'mPs']}, function (err, tiersActesFournisseurs) {

          if (err)
            cb(null, err);
          if (tiersActesFournisseurs) {
            //console.log("tiersActesFournisseurs "+tiersActesFournisseurs);
            cb(null, tiersActesFournisseurs);
          } else {
            cb(null, null);
          }
        });
      }
    }); //findByID user
  }





  /***********************************/
  /** User  password oublie Logic **/
  /***********************************/

  Appuser.initialiserMotDePasseOublie = function (id, mail, cb) {
    console.log("### Debut Mot de passe oubli� ###");
    Appuser.findById(id, function (err, appuser) {
      if (err) {
        cb(null, err);
      } else {
        console.log("mail saisi: " + mail);
        console.log("mail ldap: " + appuser.email);
        if (mail == appuser.email) {

          console.log("password ldap : " + appuser.password);

          // changer le password dans LDAP 
          var pass = Math.random().toString(36).substring(2, 15);
          console.log("pass " + pass);
          appuser.updateAttributes({ password: pass }, function (err, userUpdated) {
            if (!err) {
              console.log("pass user updated encrypted " + userUpdated.password)
              cb(null, pass);
            } else {
              cb(err, null);
            }
          });


          //Envoi du password par mail

          var msgText1 = "<br>Vous avez demander le changement de Mot de Passe Pour le Compte " + id + " .";

          msgText1 = msgText1 + "<br> Veuillez vous connecter avec votre nouveau Mot de Passe:  " + pass + " Et le changer apr�s votre connexion "
          msgText1 = msgText1 + " <br> <br> <font face='Tahoma' size=1 color='#606060'>? Ceci est un message de notification. Merci de ne pas y r?pondre  ?</font><br>";
          msgText1 = msgText1 + "<font face='Tahoma' size=1 color='#606060'>Pour faire le suivi de votre r?clamation, entrer votre r?f?rence fournie ci-dessus </font>";



          Appuser.app.models.Mail.send({
            to: mail,
            from: 'portail@tgr.gov.ma',
            subject: 'R�initialisation du mot de passe sur Application Mobile TGR',
            text: 'cliquez ',
            html: '' + msgText1


          }, function (err, mail) {
            if (!err) {
              console.log('email sent!');
              console.log(mail);

            } else {
              console.log('email not sent!');
              console.log(mail);

            }
          });


          // fin envoi de mail



        } else {
          cb(null, "ERROR");
        }
      }
    });
  }


  /*****************************/
  /**** User session **********/
  /****************************/
  Appuser.logout = function (access_token, cb) {
    if (!access_token)
      cb(null, 'no access_token specified!');
    Appuser.relations.accessTokens.modelTo.findById(access_token, function (err, accessToken) {
      if (err) {
        cb(null, err);
      } else if (accessToken) {
        accessToken.destroy(cb);
      } else {
        cb(null, 'could not find accessToken');
      }
    });
  }

  Appuser.afterRemote('findById', function (ctx, user, next) {
    //Appuser.user = user;
    if (user.jpegphoto) {
      var buf = new Buffer(user.jpegphoto, 'base64', { type: 'image/bmp' });
      fs.writeFile("/api/" + user.username + ".jpeg", buf, function (err) {
        if (err)
          cb(null, err);
        user.jpeguri = encodeURI(serverURL + "/api/" + user.username + ".jpeg");
        user.jpegphoto = null;
        next();
      });//end fs.writeFile
    } else
      next();
  });
  Appuser.beforeRemote('**', function (ctx, user, next) {
    console.log("before remote");
    if (Appuser.user && ctx.res.req.accessToken && Appuser.user.id === ctx.res.req.accessToken.userId) {
    } else if (Appuser.user && ctx.res.req.accessToken && Appuser.user.id != ctx.res.req.accessToken.userId) {
      delete Appuser.user;
    }
    console.log(ctx.instance);
    /*if (ctx.instance) {
      ctx.operation.instances = [ctx.instance];
      console.log('ctx.operation.instances='+ctx.operation.instances);
    }else{
        
    }*/

    next();
  });
  Appuser.afterRemote('**', function (ctx, user, next) {
    //console.log(ctx.res.req.accessToken);
    console.log("after remote");
    next();
  });
  Appuser.observe('loaded', function (ctx, next) {
    console.log("loaded");
    //Appuser.user = ctx.data;
    //console.log(ctx.data);
    //console.log(ctx.options);
    //ctx.instance = ctx.data;
    //console.log('ctx.instance='+JSON.stringify(ctx.instance));
    //console.log(ctx.data.codecontribuable);
    //console.log(ctx.data.norib);
    var str1 = String(ctx.data.codecontribuable);
    var list1 = str1.split(',');

    var str2 = String(ctx.data.norib);
    var list2 = str2.split(',');

    if (ctx.data && ctx.data.codecontribuable && list1.length === 1) {
      ctx.data.codecontribuable = [ctx.data.codecontribuable];
    }
    if (ctx.data && ctx.data.norib && list2.length === 1) {
      ctx.data.norib = [ctx.data.norib];
    }

    //console.log(ctx.data.codecontribuable);
    //console.log(ctx.data.norib);
    //ctx.args.options.currentUser = user;

    next();
  });
  Appuser.observe('access', function (ctx, next) {
    //console.log(ctx.options);
    //console.log('access');
    //console.log(ctx.query);
    //console.log(JSON.stringify(ctx.query));
    //console.log(ctx.data);
    next();
  });

  Appuser.observe('before **', function (ctx, next) {
    console.log(ctx.options);
    //console.log('access');
    console.log(ctx.query);
    //console.log(JSON.stringify(ctx.query));
    //console.log(ctx.data);
    if (ctx.options.setPassword === true) {

      console.log('coco');

    }
    if (ctx.instance) {
      ctx.operation.instances = [ctx.instance];
      console.log('ctx.operation.instances=' + ctx.operation.instances);
    }
    next();
  });

  //remote api 
  Appuser.remoteMethod('comptes', {
    accepts: { arg: 'id', type: 'string' },
    http: { path: '/:id/comptes', verb: 'get' },
    returns: { arg: 'comptes', type: 'string' }
  });

  Appuser.remoteMethod('contribuables', {
    accepts: { arg: 'id', type: 'string' },
    http: { path: '/:id/contribuables', verb: 'get' },
    returns: { arg: 'contribuables', type: 'string' }
  });

  Appuser.remoteMethod('editionSituationAdministrative', {
    accepts: { arg: 'id', type: 'string' },
    http: { path: '/:id/editionSituationAdministrative', verb: 'get' },
    description: "Consultation de la situation administrative de l'Agent",
    returns: { arg: 'editionSituationAdministrative', type: 'string' }
  });

  Appuser.remoteMethod('attestationSalaire', {
    accepts: { arg: 'id', type: 'string' },
    http: { path: '/:id/attestationSalaire', verb: 'get' },
    description: "Consultation de l'attestation de salaire de l'Agent",
    returns: { arg: 'attestationSalaire', type: 'string' }
  });

  /*Appuser.remoteMethod('situationPrelevements', {
          accepts: {arg: 'id', type: 'string'},
          http: {path: '/:id/situationPrelevements', verb: 'get'},
          returns: {arg: 'situationPrelevements', type: 'string'}

  });*/

  Appuser.remoteMethod('soldeBancaire', {
    accepts: [{ arg: 'id', type: 'string' }, { arg: 'rib', type: 'string' }],
    http: { path: '/:id/:rib/soldeBancaire', verb: 'get' },
    returns: { arg: 'soldeBancaire', type: 'string' }
  });

  Appuser.remoteMethod('dernieresOperations', {
    accepts: [{ arg: 'id', type: 'string' }, { arg: 'rib', type: 'string' }],
    http: { path: '/:id/:rib/dernieresOperations', verb: 'get' },
    returns: { arg: 'dernieresOperations', type: 'string' }
  });

  Appuser.remoteMethod('entreDatesOperations', {
    accepts: [{ arg: 'id', type: 'string' }, { arg: 'rib', type: 'string' }, { arg: 'date1', type: 'string' }, { arg: 'date2', type: 'string' }],
    http: { path: '/:id/:rib/entreDatesOperations', verb: 'get' },
    returns: { arg: 'entreDatesOperations', type: 'string' }
  });



  //Remote methode recupererDonneesGraphe
  Appuser.remoteMethod('recupererDonneesGraphe', {
    accepts: [{ arg: 'id', type: 'string' }, { arg: 'rib', type: 'string' }],
    http: { path: '/:id/:rib/recupererDonneesGraphe', verb: 'get' },
    returns: { arg: 'graphes', type: 'array' }
  });


  //Remote methode listeActesFournisseurs
  Appuser.remoteMethod('listeActesFournisseurs', {
    accepts: { arg: 'id', type: 'string' },
    http: { path: '/:id/tiersActesFournisseurs', verb: 'get' },
    returns: { arg: 'tiersActesFournisseurs', type: 'array' }
  });

  //Remote methode initialiserMotDePasseOublie
  Appuser.remoteMethod('initialiserMotDePasseOublie', {
    accepts: [{ arg: 'id', type: 'string' }, { arg: 'mail', type: 'string' }],
    http: { path: '/:id/:mail/initialiserMotDePasseOublie', verb: 'get' },
    returns: { arg: 'pass', type: 'string' }
  });


  Appuser.remoteMethod('consulterArticlesSoldes', {
    accepts: [{ arg: 'id', type: 'string' }, { arg: 'codecontribuable', type: 'string' }],
    http: { path: '/:id/:codecontribuable/consulterArticlesSoldes', verb: 'get' },
    returns: { arg: 'consulterArticleSoldes', type: 'string' }
  });

  Appuser.remoteMethod('consulterArticlesNonSoldes', {
    accepts: [{ arg: 'id', type: 'string' }, { arg: 'codecontribuable', type: 'string' }],
    http: { path: '/:id/:codecontribuable/consulterArticlesNonSoldes', verb: 'get' },
    returns: { arg: 'consulterArticlesNonSoldes', type: 'string' }
  });

  Appuser.remoteMethod('logout', {
    accepts: { arg: 'access_token', type: 'string' },
    http: { path: '/:access_token/logout', verb: 'get' },
    returns: { arg: 'logout', type: 'string' }
  });

};

