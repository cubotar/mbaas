'use strict';

module.exports = function(Poste) {

   var app = require('../../server/server');
   const fs = require('fs');
   var http = require('http');
   var async = require( "async" );
   //var tools = require('../../server/tools');
   var util = require('util');
      
   Poste.validatesUniquenessOf('code', {message: 'code is not unique'});
   
   Poste.on('attached', function() {
    Poste.nestRemoting('responsable');
   });

  
   //get all responsables under a direction post
   Poste.listResponsables = function(direction, cb) {
      var responsables = [];
      var postes = [];
      var serverURL = 'https://api.tgr.gov.ma';
      var cpt = 0;
      var cptb = 0;

      if (!direction){
        cb(null, 'Parametre Invalide.');
        //tools.sendError('Parametre Invalide', '422', cb); 
        //app.logger.info('Parametre Invalide.');
        return;
      }
      Poste.findById( direction, {include: [{relation: 'postesRattaches', scope:{include: [{relation: 'postesRattaches', scope: {include: ['ville', 'typePoste', 'responsable']} }, 'ville', 'typePoste', 'responsable']}} , 'ville', 'typePoste', 'responsable']} , function (err, dir) {
        if (err){
         //console.trace(err);
	 cb(null,err);
         return;
        }        
        //console.log(dir);
        if (!dir){
          cb(null,'Poste Introuvable.');
          //tools.sendError('Poste Introuvable', '401', cb);
          //app.logger.info('Poste Introuvable.');
          return;
          //cb(null,'Poste Introuvable.');
        }
        
        var direction = JSON.parse(JSON.stringify(dir));

        if (direction && direction.responsable){
        var list = direction.responsable;
        var responsable = '';
        list.forEach(function(elem) {
	    if (elem.dn.indexOf('collaborateurs') === -1)
               responsable = elem;
        });
        direction.responsable = responsable;
        responsable = '';
        }

        delete direction.postesRattaches;
        postes.push(direction);
        var divisions = JSON.parse(JSON.stringify(dir)).postesRattaches; 
        divisions.forEach(function(division) {

             if (division && division.responsable) {
             list = division.responsable;
             list.forEach(function(elem) {
                if (elem.dn.indexOf('collaborateurs') === -1)
                 responsable = elem;
             });
             division.responsable = responsable;
             responsable = '';
             }
           
             var services = JSON.parse(JSON.stringify(division)).postesRattaches;
             delete division.postesRattaches;
             postes.push(division);
             services.forEach(function(service) {
   
                 if (service && service.responsable) {
                 list = service.responsable;
             	 list.forEach(function(elem) {
                   if (elem.dn.indexOf('collaborateurs') === -1)
                     responsable = elem;
                 });
                 service.responsable = responsable;
                 responsable = '';
                 }

                 postes.push(service);
             });
        });
        postes.forEach(function(poste) {
           if (poste.responsable && poste.responsable.jpegphoto)
              cpt++;
        });

        postes.forEach(function(poste) {
           if (poste.responsable && poste.responsable.jpegphoto){
           var buf = new Buffer(poste.responsable.jpegphoto, 'base64', {type: 'image/bmp'});
           fs.writeFile("/api/"+poste.responsable.username+".jpeg", buf, function(err) {
                    if(err)
                       cb(null, err);
                     poste.responsable.jpeguri = encodeURI(serverURL+ "/api/"+poste.responsable.username+".jpeg");
                     poste.responsable.jpegphoto = null;
                     cptb++;
                     if (cpt === cptb) cb(null, postes);
            });//end fs.writeFile
           }
        });
       
        //no poste found with jpegphoto 
        if (cpt === 0)
 	   cb(null, postes);
      });//Poste.findById
      
    }

   //get all responsables under a direction post
   Poste.chercherResponsables = function(nom, cb) {
      var responsables = [];
      var postes = [];
      var cpt = 0;
      var cptb = 0;
      var cptu = 0;
      var size = 0;
      var status = 'notyet';
      var serverURL = 'https://api.tgr.gov.ma';


      if (!nom){
        cb(null, 'Parametre Invalide.');
        //tools.sendError('Parametre Invalide', '422', cb);
        //app.logger.info('Parametre Invalide.');
        return;
      }
      app.models.AppUser.find({where: {completeName: {like: '%'+nom+'%'}}}, function (err, users) {
           if (users.length === 0){
            console.log('no user found !');
            cb('nom introuvable!', null);
           }
           if (err || users.length === 0){
            console.log('unable to load users!');
            cb('impossible de charger les responsables!', null);
           }
           else{
             if (users.lenght === 0){
               console.log('unable to load users!');
               cb('impossible de charger les utilisateurs!', null);

             }
             users.forEach(function(user) {
                if (user.userOU){
                size++;
             }
             });
             users.forEach(function(user) {
                if (user.userOU){
                //cptu++;
                Poste.findById(user.userOU, {include: ['ville', 'typePoste']}, function (err, poste) {
                     cptu++;
                     if (err){
            		console.log('unable to load poste!');
                     }else{
                        if (poste){
                          cpt++;
                          if (user.jpegphoto){
                               var buf = new Buffer(user.jpegphoto, 'base64', {type: 'image/bmp'});
                               fs.writeFile("/api/"+user.username+".jpeg", buf, function(err) {
                               if(err)
                                 cb(null, err);
                     	       user.jpeguri = encodeURI(serverURL+ "/api/"+user.username+".jpeg");
                               user.jpegphoto = null;
                               var ville;
                               var typePoste;

                               for(var key in poste) {
                                   for(var innerKey in poste[key]) {
                                        //console.log("Key: " + innerKey + " value: " + poste[key][innerKey]);
                                        if (innerKey === "ville")
                                           ville = poste[key][innerKey];
                                        if (innerKey === "typePoste")
                                           typePoste = poste[key][innerKey];
                                   }
                                }
                                 var pst = {"code":poste.code, "libelle":poste.libelle, "adresse":poste.adresse, "fax":poste.fax,
                                        "secretariat":poste.secretariat, "position":poste.position, "ordre":poste.ordre,
                                        "typePosteId":poste.typePosteId, "posteRattachement":poste.posteRattachement, "villeId":poste.villeId,
                                        "ville":ville, "typePoste":typePoste, "responsable":user };
                                 poste = pst;
                                 postes.push(pst);

                               /*var pst = {"code":poste.code, "libelle":poste.libelle, "adresse":poste.adresse, "fax":poste.fax, 
                               "secretariat":poste.secretariat, "position":poste.position, "ordre":poste.ordre, 
                               "typePosteId":poste.typePosteId, "posteRattachement":poste.posteRattachement, "villeId":poste.villeId, 
				"ville":poste.ville, "typePoste":poste.typePoste, "responsable":user };
                               //poste = {"poste":poste, "responsable": user};
                               poste = pst;
                               //poste.responsable = user;
                               //postes.push(poste);
                               postes.push(pst);*/
                               cptb++;
                               if (cptu === size && status != 'finished') {status='finished';cb(null, postes);return;}

                               });//end fs.writeFile
                          }else{
		               var ville;
                               var typePoste;
	
                               for(var key in poste) {
                                   for(var innerKey in poste[key]) {
                                        //console.log("Key: " + innerKey + " value: " + poste[key][innerKey]);
                                        if (innerKey === "ville")
                                           ville = poste[key][innerKey];
                                        if (innerKey === "typePoste")
                                           typePoste = poste[key][innerKey];
                                   }
                                }
                                 var pst = {"code":poste.code, "libelle":poste.libelle, "adresse":poste.adresse, "fax":poste.fax,
                                        "secretariat":poste.secretariat, "position":poste.position, "ordre":poste.ordre,
                                        "typePosteId":poste.typePosteId, "posteRattachement":poste.posteRattachement, "villeId":poste.villeId,
                                        "ville":ville, "typePoste":typePoste, "responsable":user };
                                 poste = pst;
                                 postes.push(pst);



			       /*var pst = {"code":poste.code, "libelle":poste.libelle, "adresse":poste.adresse, "fax":poste.fax,
                               "secretariat":poste.secretariat, "position":poste.position, "ordre":poste.ordre,
                               "typePosteId":poste.typePosteId, "posteRattachement":poste.posteRattachement, "villeId":poste.villeId,
                                "ville":poste.ville, "typePoste":poste.typePoste, "responsable":user };
                               poste = pst;
                               //poste = {"poste":poste, "responsable": user};
                               //poste.responsable = user;
                               //postes.push(poste);
                               postes.push(pst);*/

                               if (cptu === size && status != 'finished') {status='finished';cb(null, postes);return;}

                               //if (cptu === size && status!= 'finished') {status='finished';cb(null, postes);return;}
                          }
                       }
                        //console.log(poste);
                     }
                 });//end Poste.findById
                 }//end if user.userOU
                 else{
                     
                 }
             });

             //cb(null, users);
           }
      });//end find
     
     
   }

    process.on('return', function() { process.exit(1); });

    Poste.remoteMethod('listResponsables', {
          accepts: {arg: 'direction', type: 'string'},
          http: {path: '', verb: 'get'},
          description: "Liste des responsables d''une direction : recherche par code direction",
          returns: {arg: 'responsables', type: 'array'}
    });

    Poste.remoteMethod('chercherResponsables', {
          accepts: {arg: 'nom', type: 'string'},
          http: {path: '', verb: 'get'},
          description: "Liste des responsables avec critere de recherche : recherche par nom",
          returns: {arg: 'responsables', type: 'array'}
    });


};
