'use strict';

var app = require('../../server/server');
var parallel = require('async/parallel');
var path = require('path');

module.exports = function (Adherent) {


    // verification dans la table adh?rent
    Adherent.validatesUniquenessOf('mail', { message: 'email is not unique' });
    Adherent.validatesUniquenessOf('login', { message: 'login is not unique' });
    Adherent.validatesPresenceOf('login', 'mail');
    Adherent.validatesLengthOf('password', { min: 8, message: { min: 'Password invalid min 8 max 20' } });
    Adherent.validatesLengthOf('login', { min: 5, max: 15, message: { min: 'login invalide: min 5 max 15' } });


    //before save adherent 
    Adherent.observe('before save', function updateTimestamp(ctx, next) {
        ctx.hookState._mailAdress = ctx.instance.mail;

        //generation du code de validation 
        var date = new Date();
        var codeV = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15).concat(date.getTime());
        console.log("codeV " + codeV);
        ctx.instance.codeValidation = codeV;
        ctx.hookState._codeValidation = codeV;


        //si fonctionnaire le login=ppr
        if (ctx.instance.ppr) {
            ctx.instance.login = ctx.instance.ppr;
            ctx.hookState._login = ctx.instance.ppr;
        } else {
            ctx.hookState._login = ctx.instance.login;
        }



        // Save etat adherent	
        if (ctx.instance._etatAdherent) {
            console.log("traitement etat adherent ");
            app.models.TypeEtatAdherent.findOne({ where: { code: 'CREE' } }, function (err, typeEtatAdherent) {
                console.log(typeEtatAdherent.code);
                if (err) {
                    next(err);
                }
                else {
                    var etatAdherentDAO = app.models.EtatAdherent;
                    var etatAdherentInstance = ctx.instance._etatAdherent;
                    etatAdherentInstance.typeEtatId = typeEtatAdherent.id;
                    console.log(etatAdherentInstance.typeEtatId);
                    ctx.instance.unsetAttribute('_etatAdherent');
                    etatAdherentDAO.create(etatAdherentInstance, function (err, etatAdherent) {
                        if (!err) {
                            ctx.instance.etatActuel(etatAdherent);
                            console.log("etat adherent cree " + etatAdherent.id);
                            next();

                        } else {
                            console.log("etat adherent non cree !! ")
                            next(err);
                        }
                    });
                }

            });
        }
        else {
            next();
        }
    });

    //after save adherent

    Adherent.observe('after save', function sendingMail(ctx, next) {

        var corpsMessage = "<br>La TGR vous remercie d'avoir utilis? l'application mobile pour proc?der ? l'inscription<br> Votre Login est : " + ctx.hookState._login + "<br>Pour finaliser votre inscription, Merci de renseigner votre code d'activation ci-dessous au niveau de votre application Mobile: <br>";
        corpsMessage = corpsMessage + "<br> <strong><font face='Tahoma' size=2> " + ctx.hookState._codeValidation + "<strong></font>"
        corpsMessage = corpsMessage + "<br>  <font face='Tahoma' size=1 color='#606060'>? Ceci est un message de notification. Merci de ne pas y r?pondre  ?</font><br>";


        console.log("********* Sending email Adh?rent*********");
        app.models.Mail.send({
            to: ctx.hookState._mailAdress,
            from: 'portail@tgr.gov.ma',
            subject: 'Cr?ation compte Mobile TGR',
            text: 'cliquez ',
            html: corpsMessage

        }, function (err, mail) {
            if (!err) {
                console.log('email sent!');
                next();
            } else {
                console.log('email not sent!');
                next(err);
            }
        });


    });

    //verifierLogin
    Adherent.verifierLogin = function (log, cb) {
        Adherent.findOne({ where: { login: log } }, function (err, user) {
            if (err) {
                cb(err, null);
            }
            else {

                var result;
                if (user != null && user.id != null) {


                    console.log("trouv? dans la table adherent !!");
                    result = true;
                    cb(null, result);
                } else {
                    app.models.AppUser.findOne({ where: { username: log } }, function (err, usr) {
                        if (err) {
                            cb(err, null);
                        } else {

                            if (usr != null && usr.username != null) {
                                console.log("trouv? dans LDAP !! ");
                                result = true;
                            } else {
                                result = false;
                            }
                            cb(null, result);
                        }
                    });
                }



            }
        });
    }

    //verifierEmail
    Adherent.verifierEmail = function (mail, cb) {
        Adherent.findOne({ where: { mail: mail } }, function (err, user) {
            if (err) {
                cb(err, null);
            }
            else {

                var result;
                if (user != null && user.id != null) {


                    console.log("trouv? dans la table adherent !!");
                    result = true;
                    cb(null, result);
                } else {
                    app.models.AppUser.findOne({ where: { email: mail } }, function (err, usr) {
                        if (err) {
                            cb(err, null);
                        } else {

                            if (usr != null && usr.email != null) {
                                console.log("trouv? dans LDAP !! ");
                                result = true;
                            } else {
                                result = false;
                            }
                            cb(null, result);
                        }
                    });
                }



            }
        });
    }



    //verifierFonctionnaire
    Adherent.verifierFonctionnaire = function (ppr, dateEntree, netMensuel, cb) {
        var result = false;
        app.models.SituationAdministrative.findById(ppr, function (err, sa) {
            if (err) {
                cb(err, null);
            }
            else {
                if (sa != null && sa.dateEntree != null && dateEntree.toISOString().slice(0, 10) == sa.dateEntree.toISOString().slice(0, 10)) {
                    app.models.SituationPecuniere.findOne({ where: { situationAdministrativeId: sa.id } }, function (err, sp) {
                        if (err) {
                            cb(err, null);
                        }

                        else if (sp != null && netMensuel == sp.netMensuel) {
                            result = true;
                            cb(null, result);

                        } else {
                            cb(null, result);
                        }

                    });
                }
                else {
                    cb(null, result);
                }

            }

        });
    }


    //verifier password
    Adherent.verifierPassword = function (login, password, cb) {

        Adherent.findOne({ where: { login: login } }, function (err, adherent) {
            if (err)
                cb(null, err);
            if (adherent != null && adherent.password != null) {
                console.log("adherent.pasword saisi " + password);
                console.log("adherent password from DB " + adherent.password);
                console.log("comparing passwords ");
                if (password === adherent.password) {
                    cb(null, true);
                } else {
                    cb(null, false);
                }
            }
            else {
                cb(null, false)
            }

        });

    };


    //Remote methode verifierPassword
    Adherent.remoteMethod('verifierPassword', {

        accepts: [
            { arg: 'login', type: 'string' },
            { arg: 'password', type: 'string' }
        ],

        http: { path: '/verifierPassword', verb: 'get' },

        returns: { arg: 'res', type: 'boolean' }

    });

    //Remote methode verifierLogin
    Adherent.remoteMethod('verifierLogin', {
        accepts: { arg: 'log', type: 'string', required: true },
        http: { path: '', verb: 'get' },
        returns: { arg: 'result', type: 'boolean' }
    });



    //Remote methode verifierEmail 
    Adherent.remoteMethod('verifierEmail', {
        accepts: { arg: 'mail', type: 'string', required: true },
        http: { path: '', verb: 'get' },
        returns: { arg: 'result', type: 'boolean' }
    });


    //Remote methode verifierFonctionnaire
    Adherent.remoteMethod('verifierFonctionnaire', {
        accepts:
        [
            { arg: 'ppr', type: 'string' },
            { arg: 'dateEntree', type: 'date' },
            { arg: 'netMensuel', type: 'number' }
        ],

        http: { path: '', verb: 'post' },
        returns: { arg: 'result', type: 'boolean' }
    });

}
