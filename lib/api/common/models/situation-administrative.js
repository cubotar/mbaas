'use strict';


module.exports = function(Situationadministrative) {

   //get situationn administrative of a ppr id
   Situationadministrative.situationAdminsitrative = function(ppr, cb) {
       var situationAdministrative = null;

       Situationadministrative.findById( ppr, {include: ['ville', 'service', 'administration', 'grade', 'fonction']}, function (err, situationadmin) {
        if (err)
          cb(null,err);
        
          
         if (situationadmin != null){
           cb(null, situationadmin);
        }else{
          cb(null, null);
        } 

        });//Situationadministrative.findById

   }

   Situationadministrative.attestationSalaire = function(ppr, cb) {

          Situationadministrative.findById( ppr, {include: [{relation: 'situationPecuniere', scope:{include: {relation: 'situationPecuniereDetails', scope: {include: ['typeRubrique', 'rubrique']} }}} , 'ville', 'service', 'administration', 'grade', 'fonction', 'enfants']} , function (err, situationadmin) {
	        if (err)
        	  cb(null,err);

         	if (situationadmin != null){
                  cb(null, situationadmin);
                }

          });//Situationadministrative.findById


  }



  Situationadministrative.situationPrelevements = function(ppr, cb) {

          Situationadministrative.findById( ppr, {include: [{relation: 'prelevements', scope: {include: {relation: 'rubrique'} } }, 'ville', 'service', 'administration', 'grade', 'fonction'] } ,function (err, situationadmin) {

                if (err)
                  cb(null,err);

                if (situationadmin != null){
                   cb(null, situationadmin);
                }

          });//Situationadministrative.findById


  }
   
   Situationadministrative.observe('loaded', function(ctx, next) {
      //console.log('loaded');
      //console.log(ctx.data);
      //Situationadministrative.sitAdmin = ctx.data;
      //console.log(ctx.data);
      //console.log(ctx);
      next();
   });
   Situationadministrative.observe('access', function(ctx, next) {
      //console.log('access');
      //console.log(ctx.query);
      //console.log(ctx);
      next();
   });
  

   Situationadministrative.beforeRemote('**', function(ctx, user, next) {
        console.log('remote');
        console.log(ctx.query);
        next();
   });


   Situationadministrative.remoteMethod('situationAdminsitrative', {
          accepts: {arg: 'id', type: 'string'},
          http: {path: '/:id/situationAdministrative', verb: 'get'},
          returns: {arg: 'situationAdministrative', type: 'string'}
    });

   Situationadministrative.remoteMethod('attestationSalaire', {
          accepts: {arg: 'id', type: 'string'},
          http: {path: '/:id/attestationSalaire', verb: 'get'},
          returns: {arg: 'attestationSalaire', type: 'string'}

  });

   Situationadministrative.remoteMethod('situationPrelevements', {
          accepts: {arg: 'id', type: 'string'},
          http: {path: '/:id/situationPrelevements', verb: 'get'},
          returns: {arg: 'situationPrelevements', type: 'string'}

  });

};
