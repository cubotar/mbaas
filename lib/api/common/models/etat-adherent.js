'use strict';


var app = require('../../server/server');
module.exports = function (Etatadherent) {


    //Enregistrer Adherent LDAP
    Etatadherent.enregistrerAdherent = function (login, passwd, codeActivation, cb) {
        
        app.models.Adherent.findOne({ where: { login: login, codeValidation: codeActivation, password: passwd } }, function (err, adherent) {
            if (err) {
                console.log("ERROR get adherent ");
                cb(null, err);
               
            }
            else if(adherent){
                console.log("adherent found");
                //Valider adherent
                if(adherent && adherent.etatActuelId){
                    console.log("adherent.etatActuelId " + adherent.etatActuelId);
                Etatadherent.findById(adherent.etatActuelId, function (err, etatAdherent) {
                    if (err) {
                        cb(err, null);
                    } else {
                        console.log("etat adherent "+ etatAdherent);
                        app.models.TypeEtatAdherent.findOne({ where: { code: 'VALIDE' } }, function (err, typeEtatAdherent) {
                            if (err) {
                                cb(err, null);
                            } else {
                                console.log("type etat adherent found "+typeEtatAdherent);
                                etatAdherent.updateAttributes({ typeEtatId: typeEtatAdherent.id }, function (err, etatAdherentUpdated) {
                                    if (!err) {
                                        console.log("etat adherent updated " + etatAdherentUpdated)

                                    } else {
                                        cb(err, null);
                                    }
                                });//end etatAdherent.updateAttributes
                            }
                        });//end app.models.TypeEtatAdherent.findOne
                    }//end else
                });//end Etatadherent.findById
            }

                //Enregistrer adherent
                console.log("adherent.login" + adherent.login);
                var appUserInstance = app.models.AppUser;
                //creation User LDAP
                console.log("uid=" + login + ",ou=adherents,ou=users, DC=TGR,DC=MA");
                console.log("adherent.login " + adherent.login);
                console.log("adherent.nom " + adherent.nom);
                console.log("adherent.cin " + adherent.cin);
                console.log("adherent.sn " + adherent.nom);
                console.log("adherent.mobile " + adherent.mobile);
                console.log("adherent.prenom " + adherent.prenom);
                console.log("adherent.fixe " + adherent.fixe);
                console.log("adherent.mail " + adherent.mail);
                console.log("adherent.password " + adherent.password);
                console.log("adherent.cn " + adherent.nom + ' ' + adherent.prenom);
                 console.log("adherent.codecontribuable " + adherent.codeContribuables);
                console.log("adherent.dn " + "uid=" + login + ",ou=adherents,ou=users,DC=TGR,DC=MA");


                if (adherent.codeContribuables[0] != null) {
                    console.log("Code contrib existe !!");
                    console.log("adherent.codeContribuables[0] "+ adherent.codeContribuables[0]);
                    appUserInstance = {
                        "username": adherent.login,
                        "cin": adherent.cin,
                        "lastName": adherent.nom,
                        "sn": adherent.nom,
                        "cn": adherent.nom + ' ' + adherent.prenom,
                        "mobile": adherent.mobile,
                        "firstName": adherent.prenom,
                        "codecontribuable": adherent.codeContribuables,
                        "fixe": adherent.fixe,
                        "email": adherent.mail,
                        "password": adherent.password,
                        "dn": "uid=" + login + ",ou=adherents,ou=users,DC=TGR,DC=MA"
                    };
                } else {
                    appUserInstance = {
                        "username": adherent.login,
                        "cin": adherent.cin,
                        "lastName": adherent.nom,
                        "sn": adherent.nom,
                        "cn": adherent.nom + ' ' + adherent.prenom,
                        "mobile": adherent.mobile,
                        "firstName": adherent.prenom,
                        
                        "fixe": adherent.fixe,
                        "email": adherent.mail,
                        "password": adherent.password,
                        "dn": "uid=" + login + ",ou=adherents,ou=users,DC=TGR,DC=MA"
                    };
                }//end if adherent.codeContribuables


                app.models.AppUser.create(appUserInstance, function (err, user) {
                    if (err) {
                        console.log(" ### failed to create user ### "+ err);
                        cb(err, null);
                    } else {
                        //Si Fonctionnaire ==> groupe : tsFONCTPPR tsFONCTPPR 
                        if (adherent != null && adherent.ppr != null) {
                            var appGroupInstanceFonct = app.models.AppGroup;
                            //reuperer le tsFONCTPPR
                            app.models.AppGroup.findById("tsFONCTPPR", function (err, resultGroupFonct) {
                                if (err) {
                                    cb(err, null);
                                } else {
                                    var newappusers = resultGroupFonct.appUsers.concat(user.dn);

                                    appGroupInstanceFonct = {
                                        "appUsers": newappusers,
                                        "id": "tsFONCTPPR ",
                                        "name": "tsFONCTPPR "
                                    }
                                    app.models.AppGroup.replaceOrCreate(appGroupInstanceFonct, function (err, grpUser) {
                                        if (!err) {
                                            console.log("***** appGroupInstanceFonct created*****");
                                        } else {
                                            cb(err, null);
                                        }
                                    });//end app.models.AppGroup.replaceOrCreate
                                }//end if (err)
                            });//end app.models.AppGroup.findById
                        }//end if(adherent!= null


                        //Si Contribuable ==> groupe : tsDossierFiscalContrib 
                        if (adherent != null && adherent.codeContribuables != null && adherent.codeContribuables.length != 0) {
                            console.log("######## tsDossierFiscalContrib : " + adherent.codeContribuables);
                            var appGroupInstanceContrib = app.models.AppGroup;
                            //recuperer le tsDossierFiscalContrib 
                            app.models.AppGroup.findById("tsDossierFiscalContrib", function (err, resultGroupContrib) {
                                if (err) {
                                    cb(err, null);
                                } else {
                                    var newusers = resultGroupContrib.appUsers.concat(user.dn);
                                    appGroupInstanceContrib = {
                                        "appUsers": newusers,
                                        "id": "tsDossierFiscalContrib ",
                                        "name": "tsDossierFiscalContrib "
                                    }
                                    app.models.AppGroup.replaceOrCreate(appGroupInstanceContrib, function (err, grpUser) {
                                        if (!err) {
                                            console.log("***** appGroupInstanceContrib created*****");
                                        } else {
                                            cb(err, null);
                                        }
                                    });//end app.models.AppGroup.replaceOrCreate
                                }//end if (err)
                            });//end app.models.AppGroup.findById		 
                        }//end if adherent.codeContribuables!= null


                        cb(null, user);
                    }//end if(err){
                });//end app.models.AppUser.create
                //password errone
            }
         //fin if adherent
        else {
            var error = new Error("Aucun adh�rent avec ces donn�es, Veuillez v�rifier les informations saisies");
				error.status = 400;
				cb(error);
        }
        });//end  app.models.Adherent.findOne
    }//end Etatadherent.enregistrerAdherent	

    // remote methode enregistrerAdherent
    Etatadherent.remoteMethod('enregistrerAdherent', {
        accepts: [
            { arg: 'login', type: 'string' }
            , { arg: 'passwd', type: 'string' }
            , { arg: 'codeActivation', type: 'string' }
        ],

        http: { path: '', verb: 'post' },
        returns: { arg: 'user', type: 'Appuser' }
    });


}
