'use strict';

module.exports = function(Typeenvoireclamation) {
  Typeenvoireclamation.validatesUniquenessOf('code', {message: 'code is not unique'});
};
