'use strict';


//var serviceAccount = require("../../server/push-credentials.json");


const caw = require("caw");
const https = require("https");

const firebaseAgent = caw("http://proxyws.tgr.net:8080", {
  protocol: "http",
});



module.exports = function(Message) {
	var app = require('../../server/server');
		
	// Message.sendNotification = function( title, body , registrationTokens , cb){

	// 	var payload = {
	// 	  notification: {
	// 	    title: title,
	// 	    body: body
	// 	  }
	// 	};

	// 	admin.messaging().sendToDevice(registrationTokens, payload)
	// 	  .then(function(response) {
	// 			if(response.failureCount > 0)
	// 				cb(null,false);
	// 			else
	// 	    	cb(null,true);
	// 	  })
	// 	  .catch(function(error) {
	// 	   	console.log('Error sending push notification: ', error);
	// 	    cb(error);
	// 	 });
	// }
	
	var normalizeIds = function(ids){
		if(!ids || !ids.length || ids.length ==0) return [];
		var _ids = [];
		for(var i=0 ; i < ids.length ; i++){
			if(ids[i].toString().indexOf(",")!=-1){
				var parts = ids[i].split(",");
				for(var j = 0 ; j < parts.length ; j++ ){
					if(parts[j].indexOf("uid")!=-1 && parts[j].indexOf("=")!=-1){
						_ids.push(parts[j].split("=")[1]);
					}
				}
			}else{
				_ids.push(ids[i]);
			}
		}

		return _ids;
	}
	// Getting userIds from groups
	var getUsersIdsFromGroup = function(groups, cb){
		var usersIds = [];
		app.models.AppGroup.find(function(error , _groups){
				for(var i =0 ; i < _groups.length; i++){
					if(groups.indexOf(_groups[i].name) != -1){
						if(_groups[i].appUsers && Array.isArray(_groups[i].appUsers) ){
							usersIds = usersIds.concat(_groups[i].appUsers);							
						}
						else if(_groups[i].appUsers && !Array.isArray(_groups[i].appUsers)){
							usersIds.push(_groups[i].appUsers);							
						}
					}
				}
				cb(normalizeIds(usersIds));
		})
	}

	var getRegistrationTokenFromGroups = function(groups , cb , globalCallback){
		var registrationTokens = [];
		getUsersIdsFromGroup(groups, function(userIds){
			app.models.Device.find(function(error, devices){
				for(var i=0 ; i < devices.length ; i ++){
					if(userIds.indexOf(devices[i].appUserId)!= -1){
						registrationTokens.push(devices[i].registrationToken);
					}
				}
				if(!registrationTokens || registrationTokens.length == 0){
					globalCallback(null, "No registration token found");
				}else{
					cb(registrationTokens);
				}
			})
		})
	}

	

	Message.sendNotificationToGroup = function(domainId, title, body, groups , cb){
		app.models.SystemDomain.findOne({where: { id : domainId} } ,function (err, domain) {
			// Checking domain configuration 
			if(!domain || !domain.pushSettings){
				cb(null,"Invalid push configuration");
			}else{

				var firebase = require("firebase-admin");
				if(firebase.apps.length ==0){
					var defaultApp = firebase.initializeApp({
						credential: firebase.credential.cert(domain.pushSettings),
						databaseURL: "http://tgr-mobile.firebaseio.com",
						agent: firebaseAgent,
					});
				}
				var appExists = false;
				var domainApp;
				for(var v=0 ; v < firebase.apps.length ; v++){
					
					if(firebase.apps[v].name==domainId){
						appExists = true;
						domainApp = firebase.apps[v];
						break;
					}
				}
				if(!appExists){
					 domainApp = firebase.initializeApp({
						credential: firebase.credential.cert(domain.pushSettings),
						databaseURL: "http://tgr-mobile.firebaseio.com",
						agent: firebaseAgent,
					} , domainId);
				}
				
				
				var payload = { notification: { title: title, body: body }};
				getRegistrationTokenFromGroups(groups,function(registrationTokens){
					if(!registrationTokens || registrationTokens.length == 0)
						cb(null,"No Registration token found");
					domainApp.messaging().sendToDevice(registrationTokens, payload).then(function(response) {
						if(response.failureCount > 0)
							cb(null,"Server error : Error while sending notification : Failed push count ="+response.failureCount);
						else
							cb(null,"ok");
					})
					.catch(function(error) {
						cb(null,"Server error : Error while sending notification "+error);
					});
				}, cb);
				
			}
		});
	}

	Message.remoteMethod('sendNotificationToGroup', {
			accepts: [
						{arg: 'domainId', type: 'string'},
						{arg: 'title', type: 'string'},
						{arg: 'content', type: 'string'},
						{arg: 'groups', type: 'array'}
						],
			http: {path: '/sendToGroup', verb: 'post'},
			returns: {arg: 'response', type: 'String'}
	});

	Message.remoteMethod('sendNotification', {
          accepts: [
          			{arg: 'title', type: 'string'},
          			{arg: 'body', type: 'string'},
          			{arg: 'registrationTokens', type: 'array'}
          			],
          http: {path: '/send', verb: 'post'},
          returns: {arg: 'response', type: 'Boolean'}
  });

};
