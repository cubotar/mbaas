'use strict';

module.exports = function (Banquegraphe) {



    //recupererDonneesGraphe id= compteid
    Banquegraphe.recupererDonneesGraphe = function (id, cb) {
        Banquegraphe.find({ where: { 'compte': id } }, function (err, data) {
            if (err) {
                console.log("erreur find data graphe");
                cb(err, null);

            } else {
                console.log("data graphe" + data);
                cb(null, data);

            }


        });
    }


    //Remote methode recupererDonneesGraphe
    Banquegraphe.remoteMethod('recupererDonneesGraphe', {
        accepts: { arg: 'id', type: 'string', required: true },
        http: { path: '', verb: 'get' },
        returns: { arg: 'data', type: 'array' }
    });


};