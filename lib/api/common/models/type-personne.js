'use strict';

module.exports = function(Typepersonne) {
  Typepersonne.validatesUniquenessOf('code', {message: 'code is not unique'});
};
