var app = require('../../server/server');
module.exports = function (Reponsereclamation) {

        Reponsereclamation.consulterReponseReclamation = function (ref, cb) {

                app.models.Reclamation.findOne({ where: { reference: ref } }, function (err, reclamation) {


                        if (err) {
                                cb(err, null);
                        }
                        else if (reclamation && reclamation.id != null) {


                                Reponsereclamation.findOne({ where: { reclamationId: reclamation.id }, include: ['typeEtatReclamation'] }, function (err, reponseReclam) {
                                        if (err) {
                                                cb(err, null);
                                        }
                                        else if (reponseReclam) {
                                                console.log(reponseReclam);
                                                var reponseReclamation = {
                                                        "reponseReclam": reponseReclam,
                                                        "reference": reclamation.reference,
                                                        "objetReclamation": reclamation.description
                                                }
                                                cb(null, reponseReclamation);
                                        } else
                                                cb(null, null);
                                });
                        }else{
                                cb(null,null);
                        }

                });

        }


        //Remote methode consulterReponseReclamation
        Reponsereclamation.remoteMethod('consulterReponseReclamation', {
                accepts: { arg: 'ref', type: 'string' },
                http: { path: '', verb: 'get' },
                returns: { arg: 'reponseReclamation', type: 'array' }
        });


}
