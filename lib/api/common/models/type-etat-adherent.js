'use strict';

module.exports = function(Typeetatadherent) {
  Typeetatadherent.validatesUniquenessOf('code', {message: 'code is not unique'});
};
