'use strict';

module.exports = function (Graphe) {



    //recupererDonneesGraphe id= compteid
    Graphe.recupererDonneesGraphe = function (id, cb) {
        Graphe.find({where: {'compteid': id}}, function (err, data) {
            if (err) {
                console.log("erreur find data graphe");
                cb(err, null);

            } else {
                console.log("data graphe" + data);
                cb(null, data);

            }


        });
    }


    //Remote methode recupererDonneesGraphe
    Graphe.remoteMethod('recupererDonneesGraphe', {
        accepts: { arg: 'id', type: 'string', required: true },
        http: { path: '', verb: 'get' },
        returns: { arg: 'data', type: 'array' }
    });


};
