'use strict';

module.exports = function(Natureoperation) {
 Natureoperation.validatesUniquenessOf('code', {message: 'code is not unique'});
};
