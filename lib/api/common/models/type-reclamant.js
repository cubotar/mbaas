'use strict';

module.exports = function(Typereclamant) {
  Typereclamant.validatesUniquenessOf('code', {message: 'code is not unique'});
  

  Typereclamant.natureReclamations = function(code, cb) {
       //returns array of objects
       Typereclamant.findOne({where:{code:code}, include: 'natureReclamations'}, function (err, typereclamant) {
        if (err)
          cb(null,err);
        if (typereclamant){
           var natures = JSON.parse(JSON.stringify(typereclamant)).natureReclamations;
           cb(null, natures);
        }else{
             cb(null, 'pas de donnees!');
         }
        });
   }


  Typereclamant.postes = function(codetypereclamant, codeville, cb) {
       //returns array of objects
       //include: {relation: 'postes', scope:{include: {relation: 'ville', scope: {where: {code: 'NATIONAL'}}}}}}
       var list = [];
       Typereclamant.findOne({where:{code:codetypereclamant}, include: {relation: 'postes', scope:{fields: ['id', 'code', 'libelle'], include: 'ville'}}}, function (err, typereclamant) {
        if (err)
          cb(null,err);
        if (typereclamant){
           var postes = JSON.parse(JSON.stringify(typereclamant)).postes;
           postes.forEach(function(poste) {
               if (poste.ville.code === codeville){
                delete poste.ville;
                list.push(poste)
               }
           });           
           cb(null, list);
        }else{
             cb(null, 'pas de donnees!');
         }
        });
   }

    Typereclamant.acteurs = function(code, cb) {
       //returns array of objects
       Typereclamant.findOne({where:{code:code}, include: {relation: 'typeActeur', scope: {include: {relation: 'acteurs', scope:{fields: ['id', 'code', 'libelle']} } } } }, function (err, typereclamant) {
        if (err)
          cb(null,err);
        if (typereclamant){
           var acteurs = JSON.parse(JSON.stringify(typereclamant)).typeActeur.acteurs;
           cb(null, acteurs);
        }else{
             cb(null, 'pas de donnees!');
         }
        });
   }
 

   /*Typereclamant.acteursParVille = function(codetypereclamant, codeville, cb) {
       //returns array of objects
       var list = [];
       Typereclamant.findOne({where:{code:codetypereclamant}, include: {relation: 'typeActeur', scope: {include: {relation: 'acteurs', scope:{fields: ['id', 'code', 'libelle'], include: 'ville' }}}}}, function (err, typereclamant) {
        if (err)
          cb(null,err);
        if (typereclamant){
           var acteurs = JSON.parse(JSON.stringify(typereclamant)).typeActeur.acteurs;

            acteurs.forEach(function(acteur) {
               if (acteur.ville && acteur.ville.code === codeville){
                delete acteur.ville;
                list.push(acteur)
               }
           });
           cb(null, list);
        }else{
             cb(null, 'pas de donnees!');
         }
        });
   }
   */


    Typereclamant.remoteMethod('postes', {
          accepts: [{arg: 'codetypereclamant', type: 'string'}, {arg: 'codeville', type: 'string'}],
          http: {path: '', verb: 'get'},
          description: ["Liste des postes par ville pour un type de reclamant choisi : ",
                        "Entrer le code du type reclamant et le code de la ville"],
          returns: {arg: 'postes', type: 'array'}
    });
    Typereclamant.remoteMethod('natureReclamations', {
          accepts: {arg: 'code', type: 'string'},
          http: {path: '', verb: 'get'},
          description: ["Liste de natures reclamations pour un type de reclamant choisi : ",
                         "Entrer le code type reclamant"],
          returns: {arg: 'natures', type: 'array'}
    });

    Typereclamant.remoteMethod('acteurs', {
          accepts: {arg: 'code', type: 'string'},
          http: {path: '', verb: 'get'},
          description: ["Liste acteurs pour un type de reclamant choisi : ",
                         "Entrer le code type reclamant"],
          returns: {arg: 'acteurs', type: 'array'}
    });

    /*Typereclamant.remoteMethod('acteursParVille', {
          accepts: [{arg: 'codetypereclamant', type: 'string'}, {arg: 'codeville', type: 'string'}],
          http: {path: '', verb: 'get'},
          description: ["Liste acteurs pour un type de reclamant choisi et une ville choisie : ",
                         "Entrer le code type reclamant et le code ville"],
          returns: {arg: 'acteurs', type: 'array'}
    });*/
};
