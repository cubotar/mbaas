'use strict';

module.exports = function(Grade) {
  Grade.validatesUniquenessOf('code', {message: 'code is not unique'});
};
