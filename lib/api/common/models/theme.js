'use strict';

module.exports = function(Theme) {
    
    Theme.validatesUniquenessOf('name');

    Theme.activateTheme = function( themeId , domainId , cb){
  
            Theme.updateAll({ systemDomainId : domainId }, { isActive : false}, function(err, info){
                if(err){
                  cb(null,false);
                }
				else{
                    Theme.updateAll({id : themeId }, { isActive : true}, function(error, model){
                        if(error){
                            cb(null,false);
                        }else{
                           cb(null,true);
                        }
                    })   
                }
                
            })
	}

	Theme.remoteMethod('activateTheme', {
          accepts: [
          			{arg: 'themeId', type: 'string'},
          			{arg: 'domainId', type: 'string'}
        ],
          http: {path: '/activate', verb: 'post'},
          returns: {arg: 'response', type: 'Boolean'}
    });
};
