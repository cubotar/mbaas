echo "TGRMbaas Deploying to clutser..."

string=`cat package.json|grep version|awk -F: '{ print $2 }'|awk -F',' '{ print $1 }'`
version=`echo $string | sed -e 's/\"//g'`

ssh root@mbaas1.tgr.gov.ma <<'ENDSSH'
rm -rf /application/prod/TGRMBaaS-*.tgz
rm -rf /application/prod/api
rm -rf /application/prod/bin
#Initial build
#mkdir -p /var/log/mbaas
#mkdir /storage
#mkdir /api
ENDSSH

ssh root@mbaas2.tgr.gov.ma <<'ENDSSH'
rm -rf /application/prod/TGRMBaaS-*.tgz
rm -rf /application/prod/api
rm -rf /application/prod/bin
#Initial build
#mkdir -p /var/log/mbaas
#mkdir /storage
#mkdir /api

ENDSSH


scp  ../TGRMBaaS-$version.tgz  root@mbaas1:/application/prod
scp  ../TGRMBaaS-$version.tgz  root@mbaas2:/application/prod


ssh root@mbaas1.tgr.gov.ma <<'ENDSSH'
cd /application/prod
tar -xvzf TGRMBaaS-*.tgz
cp /application/prod/api/html/* /api/.
ENDSSH

ssh root@mbaas2.tgr.gov.ma <<'ENDSSH'
cd /application/prod
tar -xvzf TGRMBaaS-*.tgz
cp /application/prod/api/html/* /api/.
ENDSSH


