echo "TGRMBaas App Building ..."
export NODE_TLS_REJECT_UNAUTHORIZED=0
#slc build --npm
npm install
npm prune --production
string=`cat package.json|grep version|awk -F: '{ print $2 }'|awk -F',' '{ print $1 }'`
version=`echo $string | sed -e 's/\"//g'`
cd ..
tar -cvzf TGRMBaaS-$version.tgz api/ ../bin/
cd -
