echo "TGRMbaas Cluster Restarting..."

ssh root@mbaas1.tgr.gov.ma <<'ENDSSH'
cd /application/prod/bin
./stop-api
./start-api
ENDSSH

ssh root@mbaas2.tgr.gov.ma <<'ENDSSH'
cd /application/prod/bin
./stop-api
./start-api
ENDSSH

