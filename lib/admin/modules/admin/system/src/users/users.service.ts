import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { SystemUserApi } from '@colmena/admin-lb-sdk'
export { SystemUser as User } from '@colmena/admin-lb-sdk'
import { UiDataGridService, FormService } from '@colmena/admin-ui'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Injectable()
export class UsersService extends UiDataGridService {
  public icon = 'icon-lock'
  public title = 'API Guards'
  public domains: any[] = []
  public selectedUser: any
  private apiRoot : string;
  private currentDomain : string;
  private dayOfWeek : any[] = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];

  public tableColumns = [
    // { field: 'fullName', label: 'Name', action: 'edit' },
    // { field: 'email', label: 'Email' },
    { field: 'apiRegExp', label: 'API Pattern' ,action: 'edit' },
    { field: 'groups', label: 'Allowed groups' },
    { field: 'days', label: 'Days' },
    { field: 'from', label: 'From' },
    { field: 'to', label: 'To' },
  ]

  constructor(
    public userApi: SystemUserApi,
    public formService: FormService,
    private store: Store<any>,
    private http: Http, private ui : ToastyService, private toastyConfig :ToastyConfig
  ) {

    
    super()
    this.getDomains()
    this.columns = this.tableColumns
    this.apiRoot = JSON.parse(window.localStorage.getItem('apiConfig')).url;
    this.currentDomain = JSON.parse(window.localStorage.getItem('domain')).id;
    this.toastyConfig.limit = 10;
    this.toastyConfig.theme = 'bootstrap';
  }

  setSelectedUser(user: any) {
    this.selectedUser = user
  }

  getDomains(): void {
    this.store.select('app').map(data => data['domains']).subscribe(domains => {
      this.domains = domains.map(domain => ({
        label: domain.name,
        value: domain.id,
      }))
    })
  }

  getFormFields(editForm = false) {
    const fields = [
      this.formService.input('apiRegExp', {
        label: 'API Pattern*',
        placeholder: 'API Pattern',
        
      }),
      this.formService.input('groups', {
        label: 'Allowed groups*',
        placeholder: 'Allowed groups (comma seperated values)',
      }),
      this.formService.input('days', {
        label: 'Days (values : ALL,'+this.dayOfWeek+')',
        placeholder: 'Days (comma seperated values, ALL for all days)',
      }),
      this.formService.time('from', {
        label: 'From',
        placeholder: 'From',
        required:false
      }),
      this.formService.time('to', {
        label: 'To',
        placeholder: 'To',
        required:false
      }),
    ]
    
    return fields
  }

  getFormConfig(editForm = false): any {
    return {
      icon: this.icon,
      fields: this.getFormFields(editForm),
      showCancel: true,
      hasHeader: false,
    }
  }

  getItems(): Observable<any> {
    return this.http.get(this.apiRoot+'/Guards')
    .map((res:Response) => res.json());
  }

  getItem(id): Observable<any> {
    return this.http.get(this.apiRoot+'/Guards/'+id)
    .map((res:Response) => res.json());
  }

  getItemCount(): Observable<any> {
    return this.http.get(this.apiRoot+'/Guards/count')
    .map((res:Response) => res.json());
  }

  upsertItem(item, successCb, errorCb): Subscription {
    if (item.id) {
      return this.updateUser(item, successCb, errorCb)
    }
    return this.createUser(item, successCb, errorCb)
  }

  // Edited
  createUser(item, successCb, errorCb): Subscription {
    // TODO SOME controles on item
    console.log(item.groups);
    if(item.groups && item.groups!= ""){
      item.groups = item.groups.split(',');      
    }
    else{
      item.groups = [];
    }
    if(item.days && item.days!= ""){
      item.days = item.days.split(',');      
    }else{
      item.days =[];      
    }
    return this.http.post(this.apiRoot+'/Guards/', item)
    .map((res:Response) => res.json())
    .subscribe(
      () =>     this.ui.success('New guard added successfully'), 
      () =>     this.ui.error('Could not add a new guard, check if API Regex and groups are not empty'))
  }

  updateUser(item, successCb, errorCb): Subscription {
    console.log(item);
    if(item.groups && item.groups!= "" && !Array.isArray(item.groups)){
      item.groups = item.groups.split(',');      
    }else if(item.groups && Array.isArray(item.groups)){
      // Do nothing
    }
    else{
      item.groups = [];
    }

    if(item.days && item.days!= "" && !Array.isArray(item.days)){
      item.days = item.days.split(',');      
    }else if(item.days && Array.isArray(item.days)){
      // Do nothing
    }else{
      item.days =[];      
    }

    return this.http.patch(this.apiRoot+'/Guards/'+item.id,item)
    .map((res:Response) => res.json())
    .subscribe(
      () =>   {   this.ui.success(' Guard updated successfully') },
      () =>   {   this.ui.error('Could not update current guard, check if API Regex and groups are not empty')} )
  }

// Test OK
  deleteItem(item, successCb, errorCb): Subscription {
    return  this.http.delete(this.apiRoot+'/Guards/'+item.id)
    .map((res:Response) => res.json()).subscribe(successCb, errorCb)
  }
// Edited
  addUserToRole(item, successCb, errorCb): Subscription {
    return this.userApi
      .addRole(item.user.id, item.role)
      .subscribe(successCb, errorCb)
  }

  removeUserFromRole(item, successCb, errorCb): Subscription {
    return this.userApi
      .removeRole(item.user.id, item.role)
      .subscribe(successCb, errorCb)
  }

  getUserAccessTokens(item, successCb, errorCb): Subscription {
    return this.userApi.getAccessTokens(item.id).subscribe(successCb, errorCb)
  }

  generateToken(item, successCb, errorCb): Subscription {
    return this.userApi
      .createAccessTokens(item.id)
      .subscribe(successCb, errorCb)
  }

  removeTtl(item, successCb, errorCb): Subscription {
    return this.userApi
      .updateByIdAccessTokens(item.user.id, item.token.id, { ttl: -1 })
      .subscribe(successCb, errorCb)
  }

  deleteToken(item, successCb, errorCb): Subscription {
    return this.userApi
      .destroyByIdAccessTokens(item.user.id, item.token.id)
      .subscribe(successCb, errorCb)
  }

  deleteAllTokens(item, successCb, errorCb): Subscription {
    return this.userApi
      .deleteAccessTokens(item.id)
      .subscribe(successCb, errorCb)
  }

  changePassword(item, successCb, errorCb): Subscription {
    return this.userApi.resetPassword(item).subscribe(successCb, errorCb)
  }

  resetPassword(item, successCb, errorCb): Subscription {
    return this.userApi.resetPassword(item).subscribe(successCb, errorCb)
  }
}
