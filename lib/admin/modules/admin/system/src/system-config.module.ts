import { NgModule } from '@angular/core'
import { Store } from '@ngrx/store'

const moduleName = 'system'

const link = (...links) =>  ([ '/', moduleName, ...links ])

const moduleConfig = {
  name: 'System',
  icon: 'icon-settings',
  packageName: `@colmena/module-admin-${moduleName}`,
  topLinks: [
    { weight: 110, label: 'API Management', icon: 'icon-lock', link: link('api') },
    { weight: 120, label: 'Settings', icon: 'icon-settings', link: link('settings') },
    { weight: 130, label: 'Domains', icon: 'icon-globe', link: link('domains') },
  ],
  sidebarLinks: [
    { weight: 100, type: 'title', label: 'System' },
    { weight: 110, label: 'API Management', icon: 'icon-lock', link: link('api') },
    { weight: 120, label: 'Settings', icon: 'icon-settings', link: link('settings') },
    { weight: 130, label: 'Domains', icon: 'icon-globe', link: link('domains') },
  ],
  dashboardLinks: {
    system: [
      { count: '∞', label: 'API Management', type: 'info', icon: 'icon-globe', link: link('api') },
      { count: '∞', label: 'Settings', type: 'success', icon: 'icon-settings', link: link('settings') },
      { count: '∞', label: 'Domains', type: 'warning', icon: 'icon-people', link: link('domains') },
    ]
  },
}
@NgModule()
export class SystemConfigModule {

  constructor(protected store: Store<any>) {
    this.store.dispatch({ type: 'APP_LOAD_MODULE', payload: { moduleName, moduleConfig }})
  }

}

