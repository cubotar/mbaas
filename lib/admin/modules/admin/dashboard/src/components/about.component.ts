import { Component } from '@angular/core'

@Component({
  selector: 'app-about',
  template: `
    <div class="row">
      <div class="col-md-10 offset-md-1">

        <ui-card class="branding" classNames="card-outline-secondary">
          <ui-card-content>
            <ui-logo></ui-logo>
            <h4>TGR MBaaS Administration</h4>
            <h5 class="text-center">
              <span class="text-muted text-uppercase font-weight-bold">
                <i class="fa fa-letter"></i>
                <a target="_blank" href="mailto:support@tgr.gov.ma">Contact support</a>
              </span>
            </h5>

          </ui-card-content>
        </ui-card>
      </div>
    </div>
  `,
  styles: [ `
    .branding ui-logo {
      margin: 30px 0;
      display: block;
    }

    .branding h4 {
      margin: 30px 0;
      color: #243447;
      text-align: center;
    }

    .branding h5 {
      margin: 50px 0 30px;
      text-align: center;
    }

    .branding h5 span {
      margin: 0 20px;
    }

    .giants {
      padding-bottom: 20px;
    }

    .giants p {
      margin: 20px 0 30px;
      text-align: center;
    }

    .text-center {
      text-align: center;
    }

    .card-giant {
      margin-bottom: 0;
    }

    .legal {
      font-family: Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
    }
  ` ],
})
export class AboutComponent {

  public giants = [ {
    name: 'LoopBack',
    link: 'https://loopback.io',
  }, {
    name: 'Angular',
    link: 'https://angular.io',
  }, {
    name: 'CoreUI',
    link: 'https://coreui.io',
  }, ]

}
