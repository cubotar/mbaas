import { Component } from '@angular/core'

@Component({
  selector: 'app-dashboard',
  template: `
   
    <app-system-dashboard></app-system-dashboard>
  `,
})
export class DashboardComponent {}
