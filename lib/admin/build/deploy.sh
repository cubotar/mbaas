echo "TGRMbaas Deploying to clutser..."

string=`cat package.json|grep version|awk -F: '{ print $2 }'|awk -F',' '{ print $1 }'`
version=`echo $string | sed -e 's/\"//g'`

ssh root@mbaasadmin.tgr.gov.ma <<'ENDSSH'
rm -rf /application/prod/colmena-*.tgz
rm -rf /application/prod/admin
rm -rf /application/prod/bin
#Initial build
#mkdir -p /var/log/mbaas
#mkdir /storage
#mkdir /api
# add PATH=$PATH:$HOME/bin:/root/.npm-global/bin to /root/.bash_profile

ENDSSH


scp  ../colmena-$version.tgz  root@mbaasadmin.tgr.gov.ma:/application/prod
#cp  ../colmena-$version.tgz /application/prod

ssh root@mbaasadmin.tgr.gov.ma <<'ENDSSH'
cd /application/prod
tar -xvzf colmena-*.tgz
ENDSSH



