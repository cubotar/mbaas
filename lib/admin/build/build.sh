echo "@colmena/colmena App Building ..."
export NODE_TLS_REJECT_UNAUTHORIZED=0
npm install
lerna bootstrap
npm prune --production
string=`cat package.json|grep version|awk -F: '{ print $2 }'|awk -F',' '{ print $1 }'`
version=`echo $string | sed -e 's/\"//g'`
cd ..
tar -cvzf colmena-$version.tgz admin/ ../bin/
cd -
