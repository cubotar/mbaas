import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { ThemeRoutingModule } from './theme-routing.module'
import { ThemeService } from './theme.service'

import { ThemeComponent } from './components/theme.component'
import { ThemeAddComponent } from './components/theme.add.component'
import { IndexComponent } from './components/index.component'
import { FormsModule } from '@angular/forms'
import { ToastyModule } from 'ng2-toasty';


@NgModule({
  imports: [
    CommonModule,
    ColmenaUiModule,
   ThemeRoutingModule,
   FormsModule,
   ToastyModule.forRoot()
  ],
  providers: [
    ThemeService,
  ],
  declarations: [
    ThemeComponent,
    ThemeAddComponent,
    IndexComponent,
  ],
})
export class ThemeModule { }
