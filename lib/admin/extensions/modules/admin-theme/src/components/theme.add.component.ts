import { Component, ViewEncapsulation } from '@angular/core'
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';


@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-theme',
  templateUrl: 'templates/add.html',
  styles: [`
    .active .card-block{
        background: #bae1fb !important;
    }
    ui-card-content .ui-card.card:hover {
      background: #fbf0ba;
    }

    ui-card-content .ui-card.card {
      cursor: pointer;
    }
  `]
})
export class ThemeAddComponent {

  private apiRoot : string;
  private currentDomain : string;
  private themes : any[];
  private theme : any;

  constructor(private http: Http, private ui : ToastyService, private toastyConfig :ToastyConfig){

    this.apiRoot = JSON.parse(window.localStorage.getItem('apiConfig')).url;
    this.currentDomain = JSON.parse(window.localStorage.getItem('domain')).id;
    this.themes = [];
    this.theme = {};
    this.getCurrentThemes();
    this.toastyConfig.limit = 10;
    this.toastyConfig.theme = 'bootstrap';

  }


  getCurrentThemes(){
    return this.http.get(this.apiRoot+'/Themes?filter=%7B%22where%22%3A%7B%22systemDomainId%22%3A%22'+this.currentDomain+'%22%20%7D%20%7D')
    .map((res:Response) => res.json())
    .subscribe(data => { this.themes = data});
  }

  doAdd(){

    var data = { name : this.theme.id , description : this.theme.description , isActive : false , systemDomainId : this.currentDomain ,  }
    return this.http.post(this.apiRoot+'/Themes',data)
    .map((res:Response) => res.json())
    .subscribe(
      data => {  this.ui.success('New Theme added successfully');  },
      err =>  {  this.ui.error('Error while adding a new theme: check if the name is unique'); }
    
    );
  }
  
  

}