import { Component, ViewEncapsulation } from '@angular/core'
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { UiService } from '@colmena/admin-ui';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-theme',
  templateUrl: 'templates/theme.html',
  styles: [`
    .active .card-block{
        background: #bae1fb !important;
    }
    ui-card-content .ui-card.card:hover {
      background: #fbf0ba;
    }

    ui-card-content .ui-card.card {
      cursor: pointer;
    }

    .buttonDisabled {
      opacity: 0.50; 
      cursor: not-allowed;
    }

  `]
})
export class ThemeComponent {

  private apiRoot : string;
  private currentDomain : string;
  private themes : any[];

  constructor(private http: Http, private ui : ToastyService, private toastyConfig :ToastyConfig){

    this.apiRoot = JSON.parse(window.localStorage.getItem('apiConfig')).url;
    this.currentDomain = JSON.parse(window.localStorage.getItem('domain')).id;
    this.themes = [];
    this.getCurrentThemes();

    this.toastyConfig.limit = 10;
    this.toastyConfig.theme = 'bootstrap';

  }


  getCurrentThemes(){
    return this.http.get(this.apiRoot+'/Themes?filter=%7B%22where%22%3A%7B%22systemDomainId%22%3A%22'+this.currentDomain+'%22%20%7D%20%7D')
    .map((res:Response) => res.json())
    .subscribe(data => { this.themes = data });
  }

  doDelete(id){
    this.http.delete(this.apiRoot+'/Themes/'+id)
    .map((res:Response) => res.json())
    .subscribe(data => { this.ui.success('Theme deleted successfully'); this.getCurrentThemes(); });
    
  }

  doActivate(id){ 
    if(this.isActive(id)) return;
    return this.http.post(this.apiRoot+'/Themes/activate',{ themeId : id , domainId : this.currentDomain } )
    .map((res:Response) => res.json())
    .subscribe(data => {   this.ui.success('Theme activated successfully'); this.getCurrentThemes(); });
  }

  isActive(id){
    for(var i=0; i < this.themes.length ; i++){
      if(this.themes[i].id == id && this.themes[i].isActive)
        return true;
    }
    return false;
  }
  

}