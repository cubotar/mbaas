import { Component } from '@angular/core'
import { UiTabLink } from '@colmena/admin-ui'

@Component({
  selector: 'app-dashboard',
  templateUrl: 'templates/index.html' ,
})
export class IndexComponent {

  public tabs: UiTabLink[] = [
    { icon: 'icon-picture', title: 'Themes', link: 'themes' },
    { icon: 'icon-picture', title: 'Add a new theme', link: 'addTheme' },

  ]

}
