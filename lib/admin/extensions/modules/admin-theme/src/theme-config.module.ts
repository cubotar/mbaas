import { NgModule } from '@angular/core'
import { Store } from '@ngrx/store'

const moduleName = 'theme'

const link = (...links) => ([ '/', moduleName, ...links ])

const moduleConfig = {
  name: 'Theme',
  icon: 'icon-picture',
  packageName: `@colmena/module-admin-${moduleName}`,
  topLinks: [
    // { weight: 2, label: 'Starter',   icon: 'icon-control-play', link: link() }
  ],
  sidebarLinks: [
    { weight: 4, label: 'Mobile Themes',   icon: 'icon-picture', link: link() },
  ],
  dashboardLinks: {},
}

@NgModule()
export class ThemeConfigModule {

  constructor(protected store: Store<any>) {
    this.store.dispatch({ type: 'APP_LOAD_MODULE', payload: { moduleName, moduleConfig } })
  }

}

