import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { ThemeComponent } from './components/theme.component'
import { ThemeAddComponent } from './components/theme.add.component'
import { IndexComponent } from './components/index.component'

const routes: Routes = [ {
  path: 'theme',
  data: {
    title: 'Mobile Administration',
  },
  children: [
     { path: '', component: IndexComponent, children: [
      { path: '', redirectTo: 'themes', pathMatch: 'full' },
      { path: 'themes', component: ThemeComponent, data: { title: 'Mobile themes' } },
      { path: 'addTheme', component: ThemeAddComponent, data: { title: 'Add new theme' } },
      
    ]}
  ]
} ]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemeRoutingModule { }
