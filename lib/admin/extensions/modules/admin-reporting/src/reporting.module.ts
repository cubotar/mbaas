import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { ReportingRoutingModule } from './reporting-routing.module'
import { ReportingService } from './reporting.service'

import { LoggingComponent } from './components/logging.component'
import { MonitoringComponent } from './components/monitoring.component'
import { IndexComponent } from './components/index.component'
import { FormsModule } from '@angular/forms'

@NgModule({
  imports: [
    CommonModule,
    ColmenaUiModule,
    ReportingRoutingModule,
    FormsModule
  ],
  providers: [
    ReportingService,
  ],
  declarations: [
    LoggingComponent,
    MonitoringComponent,
    IndexComponent,
  ],
})
export class ReportingModule { }
