import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { LoggingComponent } from './components/logging.component'
import { MonitoringComponent } from './components/monitoring.component'
import { IndexComponent } from './components/index.component'
import { ActivitiesComponent } from './components/activities.component'

const routes: Routes = [ {
  path: 'reporting',
  data: {
    title: 'Mobile Administration',
  },
  children: [
     { path: '', component: IndexComponent, children: [
      { path: '', redirectTo: 'monitoring', pathMatch: 'full' },
      { path: 'monitoring', component: MonitoringComponent, data: { title: 'Server monitoring' } },
      // { path: 'activities', component: ActivitiesComponent, data: { title: 'Activities' } },
      { path: 'logging', component: LoggingComponent, data: { title: 'Logging' } },
    ]}
  ]
} ]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule { }
