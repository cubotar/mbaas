import { NgModule } from '@angular/core'
import { Store } from '@ngrx/store'

const moduleName = 'reporting'

const link = (...links) => ([ '/', moduleName, ...links ])

const moduleConfig = {
  name: 'Reporting',
  icon: 'icon-chart',
  packageName: `@colmena/module-admin-${moduleName}`,
  topLinks: [
    // { weight: 2, label: 'Starter',   icon: 'icon-control-play', link: link() }
  ],
  sidebarLinks: [
    { weight: 1, type: 'title', label: 'ADMINISTRATION' },
    { weight: 2, label: 'Reporting',   icon: 'icon-chart', link: link() },
  ],
  dashboardLinks: {},
}

@NgModule()
export class ReportingConfigModule {

  constructor(protected store: Store<any>) {
    this.store.dispatch({ type: 'APP_LOAD_MODULE', payload: { moduleName, moduleConfig } })
  }

}

