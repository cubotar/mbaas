import { Component } from '@angular/core'

import { ReportingService } from '../reporting.service'

@Component({
  selector: 'app-items',
  templateUrl: 'templates/activities.html',
})
export class ActivitiesComponent {

  public config = {
    class: 'table table-bordered table-stripes',
    columns: [
      { label: 'Name', field: 'name' },
      { label: 'Description', field: 'description' },
    ],
    rowButtons: [
      { typeName: 'log', label: 'Log'}
    ]
  }
  public items = []

  constructor(private service: ReportingService) {}

  ngOnInit() {
    this.service.getItems()
      .subscribe(res => this.items = res)
  }

  handleAction($event) {
    switch ($event.type) {
      case 'log':
        return console.log($event.payload)
      default:
        console.log('Unknown action: ', $event)
    }
  }
}
