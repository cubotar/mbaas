import { Component } from '@angular/core'
import { UiTabLink } from '@colmena/admin-ui'

@Component({
  selector: 'app-dashboard',
  templateUrl: 'templates/index.html' ,
})
export class IndexComponent {

  public tabs: UiTabLink[] = [
    { icon: 'icon-graph', title: 'Server monitoring', link: 'monitoring' },
    //{ icon: 'icon-list', title: 'Activities', link: 'activities' },
    { icon: 'icon-info', title: 'Logging', link: 'logging' },
  ]

}
