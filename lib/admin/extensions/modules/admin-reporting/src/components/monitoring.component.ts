import { Component } from '@angular/core'
import {SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'templates/monitoring.html',
})
export class MonitoringComponent {

  private processList : any;
  private selectedValue : any;
  private currentUrl
  constructor(private sanitizer : DomSanitizer){
    
    this.processList = JSON.parse(window.localStorage.getItem('apiNodeProcessMonitor'));
    this.selectedValue = this.processList[0] ? this.processList[0] : {};
    this.currentUrl = this.selectedValue.url+'/appmetrics-dash/';
    console.warn(this.currentUrl)
  }

  update(){
    this.currentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.selectedValue.url+'/appmetrics-dash/');
    console.log("updating ifram to", this.currentUrl)
    
  }

}
