import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { PushComponent } from './components/push.component'
import { PushConfigComponent } from './components/push.config.component'
import { IndexComponent } from './components/index.component'

const routes: Routes = [ {
  path: 'push',
  data: {
    title: 'Mobile Administration',
  },
  children: [
     { path: '', component: IndexComponent, children: [
      { path: '', redirectTo: 'push', pathMatch: 'full' },
      { path: 'push', component: PushComponent, data: { title: 'Push Notifications' } },
      { path: 'push-config', component: PushConfigComponent, data: { title: 'Push Json Configuration' } },
      
    ]}
  ]
} ]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PushRoutingModule { }
