import { Injectable } from '@angular/core'

import { StarterItemApi } from '@colmena/admin-lb-sdk'

@Injectable()
export class PushService {

  constructor(private service: StarterItemApi) {}

  getItems() {
    return this.service.find()
  }
}
