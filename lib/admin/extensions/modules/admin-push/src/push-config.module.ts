import { NgModule } from '@angular/core'
import { Store } from '@ngrx/store'

const moduleName = 'push'

const link = (...links) => ([ '/', moduleName, ...links ])

const moduleConfig = {
  name: 'Push',
  icon: 'icon-paper-plane',
  packageName: `@colmena/module-admin-${moduleName}`,
  topLinks: [
    // { weight: 2, label: 'Starter',   icon: 'icon-control-play', link: link() }
  ],
  sidebarLinks: [
    { weight: 3, label: 'Push notifications',   icon: 'icon-paper-plane', link: link() },
  ],
  dashboardLinks: {},
}

@NgModule()
export class PushConfigModule {

  constructor(protected store: Store<any>) {
    this.store.dispatch({ type: 'APP_LOAD_MODULE', payload: { moduleName, moduleConfig } })
  }

}

