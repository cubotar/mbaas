import { Component } from '@angular/core'
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import 'rxjs/add/operator/map';

@Component({
  selector: 'push-config',
  templateUrl: 'templates/push.config.html',
})
export class PushConfigComponent {

    private apiRoot : string;
    private currentDomain : string;
    private pushConfig : string;

    constructor(private http: Http, private ui : ToastyService, private toastyConfig :ToastyConfig){
        this.apiRoot = JSON.parse(window.localStorage.getItem('apiConfig')).url;
        this.currentDomain = JSON.parse(window.localStorage.getItem('domain')).id;
        this.toastyConfig.limit = 10;
        this.toastyConfig.theme = 'bootstrap';
        this.getCurrentPushConfig();
    }

    getCurrentPushConfig(){
         this.http.get(this.apiRoot+'/Domains/'+this.currentDomain )
        .map((res:Response) => res.json())
        .subscribe(
            data => {   
                if(data&& data.pushSettings){
                    this.pushConfig = JSON.stringify(data.pushSettings); 
                }
             },
             err => {   
               
                this.ui.error('Could not retrieve push settings');
             }
        );
    }

    savePushConfig(){
        
            return this.http.patch(this.apiRoot+'/Domains/'+this.currentDomain, {  pushSettings : JSON.parse(this.pushConfig)} )
            .map((res:Response) => res.json())
            .subscribe(data => {  console.log(data); this.ui.success('Push configuration saved successfully');  });
          
    }
}
