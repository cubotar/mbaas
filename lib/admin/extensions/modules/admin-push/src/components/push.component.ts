import { Component } from '@angular/core'
import { IMultiSelectOption,IMultiSelectTexts,IMultiSelectSettings  } from 'angular-2-dropdown-multiselect';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import 'rxjs/add/operator/map';

@Component({
  selector: 'push-dash',
  templateUrl: 'templates/push.html',
})
export class PushComponent {

  private apiRoot : string;
  private currentDomain : string;
  private groups: IMultiSelectOption[];
  private optionsModel: number[];
  private mySettings:IMultiSelectSettings;
  private myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    defaultTitle: 'Select a group',
    allSelected: 'All selected',
  };
  private title:string;
  private content :string;
  private selectedGroups : any[];
  constructor(private http: Http, private ui : ToastyService, private toastyConfig :ToastyConfig){
    this.apiRoot = JSON.parse(window.localStorage.getItem('apiConfig')).url;
    this.currentDomain = JSON.parse(window.localStorage.getItem('domain')).id;
    this.toastyConfig.limit = 10;
    this.toastyConfig.theme = 'bootstrap';
    this.mySettings = {
      enableSearch: true,
      checkedStyle: 'fontawesome',
      buttonClasses: 'btn btn-outline-primary btn-block',
      dynamicTitleMaxItems: 3,
      displayAllSelectedText: true
    }
    this.title ="";
    this.content = "" ;
    this.selectedGroups = [];
    this.getGroups();
    
  }

  change(){
    console.log(this.optionsModel);
  }

  getGroups(){
    return this.http.get(this.apiRoot+'/AppRoles')
    .map((res:Response) => res.json())
    .subscribe(data => { console.log(this.groups) ; this.groups = data });
  }

  sendNotification(){
    if(!this.title || this.title=="" || !this.content || this.content =="" || this.selectedGroups.length == 0){
      this.ui.error("Error while sending notification, title, content and group should not be empty"); 
    }else{
      return this.http.post(this.apiRoot+'/Messages/sendToGroup', {  domainId : this.currentDomain , title : this.title , content : this.content , groups : this.getGroupsNames() } )
      .map((res:Response) => res.json())
      .subscribe(data => {  
        if(data && data.response == "ok")
          this.ui.success('Push notification sent!'); 
        else
          this.ui.error(data.response);
       });


      
      //this.ui.success("Push notification sent!");       
    }
  }


  getGroupsNames(){
    var groupsNames = [];
    for(var i =0 ; i < this.groups.length ; i++){
      if(this.selectedGroups.indexOf(this.groups[i].id)!=1)
        groupsNames.push(this.groups[i].name);
    }
    return groupsNames;
  }

}
