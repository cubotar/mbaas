import { Component } from '@angular/core'
import { UiTabLink } from '@colmena/admin-ui'

@Component({
  selector: 'app-dashboard',
  templateUrl: 'templates/index.html' ,
})
export class IndexComponent {

  public tabs: UiTabLink[] = [
    { icon: 'icon-people', title: 'Send a push notification', link: 'push' },
    { icon: 'icon-settings', title: 'Push Json Configuration', link: 'push-config' },
    

  ]

}
