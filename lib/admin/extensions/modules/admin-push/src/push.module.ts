import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ColmenaUiModule } from '@colmena/admin-ui'

import {FormsModule } from '@angular/forms'

import { PushRoutingModule } from './push-routing.module'
import { PushService } from './push.service'

import { PushComponent } from './components/push.component'
import { PushConfigComponent } from './components/push.config.component'

import { IndexComponent } from './components/index.component'
import { ToastyModule } from 'ng2-toasty';

import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

@NgModule({
  imports: [
    CommonModule,
    ColmenaUiModule,
   PushRoutingModule,
   FormsModule,
   ToastyModule,
   MultiselectDropdownModule
  ],
  providers: [
    PushService,
  ],
  declarations: [
    PushComponent,
    PushConfigComponent,
    IndexComponent,
  ],
})
export class PushModule { }
