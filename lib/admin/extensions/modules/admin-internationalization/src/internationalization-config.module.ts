import { NgModule } from '@angular/core'
import { Store } from '@ngrx/store'

const moduleName = 'internationalization'

const link = (...links) => ([ '/', moduleName, ...links ])

const moduleConfig = {
  name: 'Internationalization',
  icon: 'icon-globe-alt',
  packageName: `@colmena/module-admin-${moduleName}`,
  topLinks: [
    // { weight: 2, label: 'Starter',   icon: 'icon-control-play', link: link() }
  ],
  sidebarLinks: [
    { weight: 5, label: 'Internationalization',   icon: 'icon-globe-alt', link: link() },
  ],
  dashboardLinks: {},
}

@NgModule()
export class InternationalizationConfigModule {

  constructor(protected store: Store<any>) {
    this.store.dispatch({ type: 'APP_LOAD_MODULE', payload: { moduleName, moduleConfig } })
  }

}

