import { Component, ViewEncapsulation } from '@angular/core'
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';


@Component({
  encapsulation: ViewEncapsulation.None,  
  selector: 'app-internationalization',
  templateUrl: 'templates/internationalization.html',
})
export class InternationalizationComponent {

  private apiRoot : string;
  private currentDomain : string;
  private translations : any[];
  private currentTranslation : any;
  private newTranslation : string;
  private currentJson : string;
 

  constructor(private http: Http, private ui : ToastyService, private toastyConfig :ToastyConfig){
    this.apiRoot = JSON.parse(window.localStorage.getItem('apiConfig')).url;
    this.currentDomain = JSON.parse(window.localStorage.getItem('domain')).id;
    this.currentTranslation = { language : "", translation :""};
    this.newTranslation = "";
    this.toastyConfig.limit = 10;
    this.toastyConfig.theme = 'bootstrap';
    this.getTranslations();
  }


  getTranslations(tr?){
    return this.http.get(this.apiRoot+'/Traductions')
    .map((res:Response) => res.json())
    .subscribe(data => { 
      this.translations = data;
      if(tr){
        this.currentTranslation = tr;
        this.currentJson = tr.translation;
      }
      else if(this.translations && this.translations.length != 0){
        this.currentTranslation = this.translations[0];
        this.currentJson = JSON.stringify(this.currentTranslation.translation,null,4);
      }
    });
  }

  doSelectTranslation(id){
    for(var i=0; i < this.translations.length; i ++ ){
      if(this.translations[i].id == id){
        this.currentTranslation = this.translations[i];
        this.currentJson = JSON.stringify(this.currentTranslation.translation,null,4);
      }
    }
  }

  doSave(id){
    this.http.patch(this.apiRoot+'/Traductions/'+this.currentTranslation.id, { translation : JSON.parse(this.currentJson) })
    .map((res:Response) => res.json())
    .subscribe(data => { 
            this.ui.success('Translation saved'); 
            this.getTranslations(data); 
            this.currentTranslation = data;
            if(this.currentTranslation.translation){
              this.currentTranslation.translation = JSON.stringify(this.currentTranslation.translation);
            }
           },

                err => { this.ui.error('Could not save translation : check if translation is json formatted'); }
    );

  }

  doAdd(){
    this.http.post(this.apiRoot+'/Traductions/', { language : this.newTranslation })
    .map((res:Response) => res.json())
    .subscribe(data => { this.ui.success('New Translation created'); this.getTranslations(data); this.newTranslation=""; },
                err => { this.ui.error('Could not add new translation : translation already exists'); }
    );
  }

  
  doDelete(id){
    this.http.delete(this.apiRoot+'/Traductions/'+id)
    .map((res:Response) => res.json())
    .subscribe(data => { this.ui.success('Translation deleted successfully'); this.getTranslations(); });
    
  }

  

}
