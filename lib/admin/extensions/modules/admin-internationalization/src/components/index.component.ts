import { Component } from '@angular/core'
import { UiTabLink } from '@colmena/admin-ui'

@Component({
  selector: 'app-internationalization',
  templateUrl: 'templates/index.html' ,
})
export class IndexComponent {

  public tabs: UiTabLink[] = [
    { icon: 'icon-globe-alt', title: 'Internationalization', link: 'internationalization' },

  ]

}
