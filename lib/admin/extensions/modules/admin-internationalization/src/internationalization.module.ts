import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ColmenaUiModule } from '@colmena/admin-ui'

import { InternationalizationRoutingModule } from './internationalization-routing.module'


import {  InternationalizationComponent } from './components/internationalization.component'
import { IndexComponent } from './components/index.component'
import { ToastyModule } from 'ng2-toasty';
import { FormsModule } from '@angular/forms'

@NgModule({
  imports: [
    CommonModule,
    ColmenaUiModule,
   InternationalizationRoutingModule,
   ToastyModule.forRoot(),
   FormsModule
  ],
  providers: [
    
  ],
  declarations: [
    InternationalizationComponent,
    IndexComponent,
  ],
})
export class InternationalizationModule { }
