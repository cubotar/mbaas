import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { InternationalizationComponent } from './components/internationalization.component'
import { IndexComponent } from './components/index.component'

const routes: Routes = [ {
  path: 'internationalization',
  data: {
    title: 'Mobile Administration',
  },
  children: [
     { path: '', component: IndexComponent, children: [
      { path: '', redirectTo: 'internationalization', pathMatch: 'full' },
      { path: 'internationalization', component: InternationalizationComponent, data: { title: 'Internationalization' } },
    ]}
  ]
} ]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InternationalizationRoutingModule { }
