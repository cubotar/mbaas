import { Component } from '@angular/core'
import { Store } from '@ngrx/store'

@Component({
  selector: 'layout-footer',
  template: `
    <footer class="footer">
       <!-- <span class="float-xs-right">TGR 2017</span> -->
    </footer>
  `,
})
export class FooterComponent {

  public footerLeft = ''
  public footerRight = ''

  constructor(private store: Store<any>) {
    this.store
      .select('layout')
      .subscribe((res: any) => {
        this.footerLeft = res.footerLeft
        this.footerRight = res.footerRight
      })
  }
}
