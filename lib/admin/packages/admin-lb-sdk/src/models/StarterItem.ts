/* tslint:disable */

declare var Object: any;
export interface StarterItemInterface {
  "id"?: string;
  "name"?: string;
  "description"?: string;
}

export class StarterItem implements StarterItemInterface {
  "id": string;
  "name": string;
  "description": string;
  constructor(data?: StarterItemInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `StarterItem`.
   */
  public static getModelName() {
    return "StarterItem";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of StarterItem for dynamic purposes.
  **/
  public static factory(data: StarterItemInterface): StarterItem{
    return new StarterItem(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'StarterItem',
      plural: 'StarterItems',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
