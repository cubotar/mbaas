/* tslint:disable */

declare var Object: any;
export interface SystemUserInterface {
  "id": string;
  "username": string;
  "jpegphoto"?: any;
  "title"?: string;
  "cin"?: string;
  "norib"?: string;
  "userOU"?: string;
  "userRAT"?: string;
  "mobile"?: string;
  "email": string;
  "firstName": string;
  "lastName": string;
  "fullName"?: string;
  "avatar"?: string;
  "codeadministration"?: string;
  "codecontribuable"?: string;
  "codeservice"?: string;
  "sexe"?: string;
  "sn"?: string;
  "commentaires"?: string;
  "titlecode"?: string;
  "titlehierarchy"?: string;
  "employeenumber"?: string;
  "uidagtsaisie"?: string;
  "telephonenumber"?: string;
  "preferredlanguage"?: string;
  "fixe"?: string;
  "realm"?: string;
  "emailVerified"?: boolean;
  "created"?: Date;
  "modified"?: Date;
  accessTokens?: any[];
  roles?: any[];
}

export class SystemUser implements SystemUserInterface {
  "id": string;
  "username": string;
  "jpegphoto": any;
  "title": string;
  "cin": string;
  "norib": string;
  "userOU": string;
  "userRAT": string;
  "mobile": string;
  "email": string;
  "firstName": string;
  "lastName": string;
  "fullName": string;
  "avatar": string;
  "codeadministration": string;
  "codecontribuable": string;
  "codeservice": string;
  "sexe": string;
  "sn": string;
  "commentaires": string;
  "titlecode": string;
  "titlehierarchy": string;
  "employeenumber": string;
  "uidagtsaisie": string;
  "telephonenumber": string;
  "preferredlanguage": string;
  "fixe": string;
  "realm": string;
  "emailVerified": boolean;
  "created": Date;
  "modified": Date;
  accessTokens: any[];
  roles: any[];
  constructor(data?: SystemUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `SystemUser`.
   */
  public static getModelName() {
    return "SystemUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of SystemUser for dynamic purposes.
  **/
  public static factory(data: SystemUserInterface): SystemUser{
    return new SystemUser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'SystemUser',
      plural: 'AppUsers',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "jpegphoto": {
          name: 'jpegphoto',
          type: 'any'
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "cin": {
          name: 'cin',
          type: 'string'
        },
        "norib": {
          name: 'norib',
          type: 'string'
        },
        "userOU": {
          name: 'userOU',
          type: 'string'
        },
        "userRAT": {
          name: 'userRAT',
          type: 'string'
        },
        "mobile": {
          name: 'mobile',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "firstName": {
          name: 'firstName',
          type: 'string'
        },
        "lastName": {
          name: 'lastName',
          type: 'string'
        },
        "fullName": {
          name: 'fullName',
          type: 'string'
        },
        "avatar": {
          name: 'avatar',
          type: 'string'
        },
        "codeadministration": {
          name: 'codeadministration',
          type: 'string'
        },
        "codecontribuable": {
          name: 'codecontribuable',
          type: 'string'
        },
        "codeservice": {
          name: 'codeservice',
          type: 'string'
        },
        "sexe": {
          name: 'sexe',
          type: 'string'
        },
        "sn": {
          name: 'sn',
          type: 'string'
        },
        "commentaires": {
          name: 'commentaires',
          type: 'string'
        },
        "titlecode": {
          name: 'titlecode',
          type: 'string'
        },
        "titlehierarchy": {
          name: 'titlehierarchy',
          type: 'string'
        },
        "employeenumber": {
          name: 'employeenumber',
          type: 'string'
        },
        "uidagtsaisie": {
          name: 'uidagtsaisie',
          type: 'string'
        },
        "telephonenumber": {
          name: 'telephonenumber',
          type: 'string'
        },
        "preferredlanguage": {
          name: 'preferredlanguage',
          type: 'string'
        },
        "fixe": {
          name: 'fixe',
          type: 'string'
        },
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "created": {
          name: 'created',
          type: 'Date'
        },
        "modified": {
          name: 'modified',
          type: 'Date'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: ''
        },
        roles: {
          name: 'roles',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
