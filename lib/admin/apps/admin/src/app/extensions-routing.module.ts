import { NgModule } from '@angular/core'

import { Routes, RouterModule } from '@angular/router'

const routes: Routes = [
  { path: '', loadChildren: '@tgr/module-admin-reporting#ReportingModule' },
  { path: '', loadChildren: '@tgr/module-admin-push#PushModule' },
  { path: '', loadChildren: '@tgr/module-admin-theme#ThemeModule' },
  { path: '', loadChildren: '@tgr/module-admin-internationalization#InternationalizationModule' },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtensionsRoutingModule { }
