import { NgModule } from '@angular/core'

import { ExtensionsRoutingModule } from './extensions-routing.module'

import { ReportingConfigModule } from '@tgr/module-admin-reporting'

import { PushConfigModule } from '@tgr/module-admin-push'

import { ThemeConfigModule } from '@tgr/module-admin-theme'

import { InternationalizationConfigModule } from '@tgr/module-admin-internationalization'


@NgModule({
  imports: [
    ExtensionsRoutingModule,
     ReportingConfigModule,
     PushConfigModule,
     ThemeConfigModule,
     InternationalizationConfigModule
  ],
})
export class ExtensionsModule {}
